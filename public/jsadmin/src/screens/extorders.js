function extorders_show() {
	gui_showLoading();
	vue.screen.data = {extOrders: [], sort: "date", filterVisible: "visible"};
	storage_open(function(event) {
		storage_readStore("extorders", function(extOrders) {
			vue.screen.data.extOrders = extOrders;
			vue.screen.component = "vue-extorder-list"
			storage_close();
			gui_hideLoading();
		});
	});
}

function extorders_showExtOrder(id) {
	gui_showLoading();
	storage_open(function(event) {
		storage_readStore("categories", function(categories) {
			if (id != null) {
				storage_get("extorders", parseInt(id), function(extOrder) {
					_extorders_showExtOrder(extOrder, categories);
					storage_close();
				});
			} else {
				storage_close();
				_extorders_showExtOrder(ExtOrder_default(), categories);
			}
		});
	});
}
function _extorders_showExtOrder(extOrder, categories) {
	vue.screen.data = {
		extOrder: extOrder,
		categories: categories,
	}
	vue.screen.component = "vue-extorder-form";
	gui_hideLoading();
}

function extorder_saveExtOrder() {
	let xo = vue.screen.data.extOrder;
	gui_showLoading();
	// Override to send date as timestamp without messing with local data
	if (xo.date != null) {
		xo.date.toJSON = function() { return xo.date.getTime() / 1000; };
	}
	if (xo.endDate != null) {
		xo.endDate.toJSON = function() { return xo.endDate.getTime() / 1000; };
	}
	if ("id" in xo) {
		srvcall_post("api/extorder", xo, extorder_saveCallback);
	} else {
		srvcall_put("api/extorder/" + encodeURIComponent(xo["reference"]), xo, extorder_saveCallback);
	}
}

function extorder_saveCallback(request, status, response) {
	if (srvcall_callbackCatch(request, status, response, extorder_saveExtOrder)) {
		return;
	}
	if (status == 400) {
		if (request.statusText == "Reference is already taken") {
			gui_showError("La référence existe déjà, veuillez en choisir une autre.");
			document.getElementById("edit-reference").focus(); // TODO: make this Vuejsy.
		} else {
			gui_showError("Quelque chose cloche dans les données du formulaire. " + request.statusText);
		}
		gui_hideLoading();
		return;
	}
	let xo = vue.screen.data.extOrder;
	let respXo = JSON.parse(response);
	if (!("id" in xo)) {
		xo.id = respXo["id"];
	}
	// Stay in sync with the server's date format
	if (xo.date != null) {
		xo.date = respXo.date;
	}
	if (xo.endDate != null) {
		xo.endDate = respXo.endDate;
	}
	storage_open(function(event) {
		storage_write("extorders", xo,
			appData.localWriteDbSuccess, appData.localWriteDbError);
	}, appData.localWriteOpenDbError);
}

