Vue.component("vue-extorder-list", {
  props: ["data"],
  data: function () {
    return {
      xoTable: {
        reference: "extorder-list",
        columns: [
          {
            reference: "reference",
            label: "Référence",
            visible: true,
            help: "La référence doit être unique pour chaque commande.",
          },
          {
            reference: "date",
            label: "Date",
            visible: true,
            help: "La date de la commande. Sa signification dépend de votre usage (date de prise de commande, début de rendez-vous…). Peut être vide.",
          },
          {
            reference: "endDate",
            label: "Date de fin",
            visible: false,
            help: "Autre champ de date. Sa signification dépend de votre usage (date prévue de retrait, fin de rendez-vous…). Peut être vide.",
          },
          {
            reference: "customerName",
            label: "Nom du client",
            visible: true,
            help: "Le nom du client assigné à la commande. Peut être vide.",
          },
          {
            reference: "custCount",
            label: "Convives",
            visible: false,
            help: "Le nombre de personnes associé à la commande. Peut être vide.",
          },
          {
            reference: "userName",
            label: "Operateurice",
            visible: false,
            help: "Le nom de l'opérateurice qui a créé la commande. Peut être vide.",
          },
          {
            reference: "visible",
            label: "Active",
            visible: true,
            help: "Indique si la commande est active ou non.",
          },
          {
            reference: "operation",
            label: "Opération",
            export: false,
            visible: true,
          },
        ],
        lines: [],
      },
      sorting: this.data.sort,
      filterVisible: this.data.filterVisible,
    };
  },
  template: `<div class="extorder-list">
<section class="box box-medium">
	<header>
		<nav class="browser">
			<ul>
				<li><a href="?p=home">Accueil</a></li>
				<li><h1>Liste des commandes</h1></li>
			</ul>
		</nav>
		<nav class="navbar">
			<ul>
				<li><a class="btn btn-add" href="?p=extorder">Ajouter une commande</a></li>
			</ul>
			<ul>
				<li>
					<label for="filter-invisible">État</label>
					<select id="filter-invisible" v-model="filterVisible">
						<option value="visible">Actives</option>
						<option value="invisible">Inactives</option>
						<option value="all">Toutes</option>
					</select>
				</li>
				<li>
					<label for="sort">Trier par</label>
					<select id="sort" name="sort" v-model="data.sort" v-on:change="sort">
						<option value="reference">Référence</option>
						<option value="customerName">Client</option>
						<option value="date">Date</option>
						<option value="endDate">Date de fin</option>
						<option value="user">Opérateurice</option>
					</select>
				</li>
			</ul>
		</nav>
	</header>
	<div class="box-body">
		<vue-table v-bind:table="xoTable"></vue-table>
	</div>
</section>
</div>`,
  methods: {
    editUrl: function (xo) {
      return "?p=extorder&id=" + xo.id;
    },
    sort: function (event) {
      let lines = [];
      for (let i = 0; i < this.data.extOrders.length; i++) {
        let xo = this.data.extOrders[i];
        let line = [
          xo.reference,
          xo.date,
          xo.endDate,
          xo.customerName,
          xo.custCount,
          xo.userName,
          { type: "bool", value: xo.visible },
          {
            type: "html",
            value:
              '<div class="btn-group pull-right" role="group"><a class="btn btn-edit" href="' +
              this.editUrl(xo) +
              '">Modifier</a></div>',
          },
        ];
        console.info(line);
        lines.push(line);
      }
      switch (this.data.sort) {
        case "reference":
          lines = lines.sort(tools_sort(0));
          break;
        case "customerName":
          lines = lines.sort(tools_sort(3));
          break;
        case "date":
          lines = lines.sort(tools_sort(1));
          break;
        case "endDate":
          lines = lines.sort(tools_sort(2));
          break;
        case "user":
          lines = lines.sort(tools_sort(4));
          break;
      }
      Vue.set(this.xoTable, "lines", lines);
    },
  },
  mounted: function () {
    this.sort();
  },
});

Vue.component("vue-extorder-form", {
  props: ["data"],
  data: function () {
    return {
      productCache: [],
    };
  },
  template: `<div class="extorder-form">
<section class="box box-medium">
	<header>
		<nav class="browser">
			<ul>
				<li><a href="?p=home">Accueil</a></li>
				<li><a href="?p=extorders">Liste des commandes</a></li>
				<li><h1>Édition d'une commande</h1></li>
			</ul>
		</nav>
	</header>
	<article class="box-body">
		<form id="edit-extorder-form" class="form-large" onsubmit="javascript:extorder_saveExtOrder(); return false;">
			<div class="form-group">
				<label for="edit-reference">Référence</label>
				<input id="edit-reference" type="text" v-model="data.extOrder.reference" required="true" />
			</div>
			<div class="form-group">
				<label for="edit-date">Date</label>
				<vue-inputdate id="edit-date" v-model="data.extOrder.date" />
			</div>
			<div class="form-group">
				<label for="edit-endDate">Date de fin</label>
				<vue-inputdate id="edit-endDate" v-model="data.extOrder.endDate" />
			</div>
			<div class="form-group">
				<label for="edit-customerName">Nom du client</label>
				<input id="edit-customerName" type="text" v-model="data.extOrder.customerName" />
			</div>
			<div class="form-group">
				<label for="edit-userName">Opérateurice</label>
				<input id="edit-userName" type="text" v-model="data.extOrder.userName" />
			</div>
			<div class="form-group">
				<input id="edit-visible" type="checkbox" v-model="data.extOrder.visible">
				<label for="edit-visible">Active</label>
			</div>

			<h2>Contenu</h2>

			<vue-catalog-picker v-bind:categories="data.categories" v-bind:prdPickCallback="pickProduct" />

			<table>
				<col />
				<col style="width:10%; min-width: 5em;" />
				<col style="width:10%; min-width: 5em;" />
				<thead>
					<tr>
						<th>Désignation</th>
						<th>Quantité</th>
						<th>Prix</th>
						<th>Opération</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="line in data.extOrder.lines">
						<td><img class="thumbnail thumbnail-text"  v-bind:src="imageSrc(line.product)" />{{line.productLabel}}</td>
						<td><input type="number" v-model.number="line.quantity" /></td>
						<td>{{ line.customUnitPrice }}</td>
						<td><button type="button" class="btn btn-delete" v-on:click="deletePrice(price.product)">X</button></a></td>
					</tr>
				</tbody>
			</table>
			<pre>{{data.extOrder | pretty}}</pre>
			<div class="form-control">
				<button class="btn btn-primary btn-send" type="submit">Enregistrer</button>
			</div>
		</form>
	</article>
</section>
</div>`,
  methods: {
    imageSrc: function (cat) {
      return "";
      /*
			if (cat.hasImage) {
				return login_getHostUrl() + "/api/image/category/" + cat.id + "?Token=" + login_getToken();
			} else {
				return login_getHostUrl() + "/api/image/category/default?Token=" + login_getToken();
			}
			*/
    },
    pickProduct: function (product) {
      this.productCache[product.id] = product;
      let line = ExtOrderLine_default(product);
      this.data.extOrder.lines.push(line);
    },
  },
});
