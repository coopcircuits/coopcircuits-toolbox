function Customer_default() {
	return {
		"dispName": "",
		"card": "",
		"firstName": "",
		"lastName": "",
		"email": "",
		"phone1": "",
		"phone2": "",
		"fax": "",
		"addr1": "",
		"addr2": "",
		"zipCode": "",
		"city": "",
		"region": "",
		"country": "",
		"tax": null,
		"discountProfile": null,
		"tariffArea": null,
		"balance": 0.0,
		"maxDebt": 0.0,
		"note": "",
		"visible": true,
		"expireDate": null,
		"hasImage": false
	};
}

