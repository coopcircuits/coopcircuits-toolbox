function ExtOrder_default() {
	return {
		"date": null,
		"endDate": null,
		"reference": "",
		"customerName": "",
		"userName": "",
		"custCount": null,
		"discountRate": 0.0,
		"visible": true,
		"lines": [],
		"customer": null,
		"tariffArea": null,
		"discountProfile": null,
		"user": null
	};
}

function ExtOrderLine_default(product) {
	if (arguments.length < 1) {
		product = null;
	}
	let line = {
		"dispOrder": 0,
		"productLabel": "",
		"product": null,
		"customUnitPrice": null,
		"customTax": null,
		"quantity": 1.0,
		"discountRate": 0.0,
	};
	if (product != null) {
		if (typeof product == "string") {
			line.productLabel = product;
		} else {
			line.productLabel = product.label;
			line.product = product.id;
		}
	}
	return line;
}
