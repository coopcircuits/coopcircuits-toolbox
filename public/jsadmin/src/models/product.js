function Product_default(categoryId, taxId) {
	return {
		"label": "",
		"hasImage": false,
		"category": categoryId,
		"dispOrder": 0,
		"visible": true,
		"prepay": false,
		"priceSell": 0.0,
		"tax": taxId,
		"priceBuy": "",
		"scaled": false,
		"reference": "",
		"barcode": "",
		"scaleType": 0,
		"scaleValue": 1.0,
		"discountEnabled": false,
		"discountRate": 0.0,
		"composition": false,
		"taxedPrice": 0.0
	};
}

function Composition_default(categoryId, taxId) {
	let prd = Product_default(categoryId, taxId);
	prd.composition = true;
	prd.compositionGroups = [];
	return prd;
}

function CompositionGroup_default() {
	return {
		"label": "",
		"dispOrder": 0,
		"compositionProducts": [],
	}
}

function CompositionProduct_default(product) {
	return {
		"dispOrder": product.dispOrder,
		"product": product.id,
	}
}
