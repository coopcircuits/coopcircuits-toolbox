# CoopCircuits Toolbox

This toolbox is a set of complementary tools and integrations to the [Open Food Network main software](https://github.com/openfoodfoundation/openfoodnetwork).

# Contributing

If you are interested in contributing drop us a line at bonjour@coopcircuits.fr :-)

# Licence

Copyright (c) 2022 CoopCircuits, released under the AGPL v3 licence.
