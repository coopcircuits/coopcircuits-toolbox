import nc from "next-connect";
import { createProxyMiddleware } from "http-proxy-middleware";
import { HEADER_API_VERSION } from "../../../lib/ofn-api";

const proxyMiddleware = createProxyMiddleware({
  target: `${process.env.COOPCIRCUITS_HOST}`,
  changeOrigin: true,
  pathRewrite: (path, req) => {
    let version = req.headers[HEADER_API_VERSION]
      ? req.headers[HEADER_API_VERSION]
      : "v0";
    return path.replace("/api/ofn-proxy/", `/api/${version}/`);
  },
  secure: false,
  followRedirects: true,
});
const handler = nc()
  /*.get(async (req, res: NextApiResponse) => {
    res.status(500).send("message");
  })
  */
  .use(proxyMiddleware);

export default handler;
