import Image from "next/image";
import Link from "next/link";
import Layout from "../components/Layout";

export default function Home() {
  return (
    <Layout pathname="/">
      <Link href="/pasteque">
        <a className="block mx-5 xl:mx-0 max-w-md py-4 px-8 bg-white shadow-lg rounded-lg my-20 cursor-pointer">
          <div>
            <Image
              src="/pasteque-logo.png"
              alt="Pastèque"
              width={45}
              height={45}
            />
            <h2 className="text-gray-800 text-3xl font-semibold inline-block ml-2">
              Pastèque
            </h2>
            <p className="mt-2 text-gray-600">
              Synchronisez CoopCircuits et Pastèque en un clic.
            </p>
          </div>
          <div className="flex justify-end mt-4">
            <span className="text-3xl font-medium text-cc-blue">
              C'est parti
            </span>
          </div>
        </a>
      </Link>
    </Layout>
  );
}
