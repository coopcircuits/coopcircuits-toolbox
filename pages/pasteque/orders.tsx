import PastequeLayout from "../../containers/pasteque/Layout";
import OrdersZone from "../../containers/pasteque/orders/OrdersZone";
import useLoggedIn from "../../lib/hooks/pasteque/useLoggedIn";

const OrdersPage = () => {
  useLoggedIn(true);
  return (
    <PastequeLayout subapp="orders">
      <OrdersZone />
    </PastequeLayout>
  );
};

export default OrdersPage;
