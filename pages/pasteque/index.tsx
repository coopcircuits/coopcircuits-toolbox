import PastequeLayout from "../../containers/pasteque/Layout";
import OutlineLink from "../../components/OutlineLink";
import Link from "next/link";
import useLoggedIn from "../../lib/hooks/pasteque/useLoggedIn";

const Pasteque = ({}) => {
  useLoggedIn(false);
  return (
    <PastequeLayout>
      <div className="max-w-3xl mx-auto">
        <div className="text-4xl">Bienvenue !</div>
        <div className="text-xl font-extralight mt-4">
          Cette plateforme sert de passerelle entre Coopcircuits et Pastèque.
          Elle est gérée par l’équipe de Coopcircuits, et vous permet de
          transférer la liste de vos produits ainsi que la liste de vos
          commandes à Pastèque, caisse enregistreuse en ligne.
        </div>
        <div className="flex justify-between mt-5">
          <Link href="/pasteque/products" passHref>
            <OutlineLink color="blue">Synchroniser les Produits</OutlineLink>
          </Link>
          <Link href="/pasteque/orders" passHref>
            <OutlineLink color="orange">Synchroniser les Commandes</OutlineLink>
          </Link>
        </div>
      </div>
    </PastequeLayout>
  );
};

export default Pasteque;
