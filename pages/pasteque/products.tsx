import PastequeLayout from "../../containers/pasteque/Layout";
import ExportProductsZone from "../../containers/pasteque/products/export/ExportProductsZone";
import ImportProductsZone from "../../containers/pasteque/products/import/ImportProductsZone";
import ProductsZone from "../../containers/pasteque/products/ProductsZone";
import useLoggedIn from "../../lib/hooks/pasteque/useLoggedIn";

const ProductsPage = () => {
  useLoggedIn(true);
  return (
    <PastequeLayout subapp="products">
      <ImportProductsZone />
      <ExportProductsZone />
      <div className="mt-8">
        <ProductsZone />
      </div>
    </PastequeLayout>
  );
};

export default ProductsPage;
