## Description
<!-- Provide a more detailed introduction to the issue itself, and why you consider it to be a bug -->
<!-- How has this bug affected you? What were you trying to accomplish? -->


## Steps to Reproduce
<!-- Provide an unambiguous set of steps to reproduce this bug -->
<!-- Include links -->
<!-- Include user ID -->

1.
2.
3.
4.

## Animated Gif/Screenshot
<!-- Provide a screenshot or brief video reproducing the bug.  -->
<!-- Please try to have the dev tools opened on the network tab (press F12 to open the devtools of your browser -->

## Severity
<!-- Assign a label and explain the impact.

bug-s1: a critical feature is broken: checkout, payments, signup, login
bug-s2: a non-critical feature is broken, no workaround
bug-s3: a feature is broken but there is a workaround
bug-s4: it's annoying, but you can use it/only a few user impacted

## Workaround
<!-- Include a workaround for this bug (if relevant) -->

## Your Environment
<!-- Include relevant details about the environment you experienced the bug in -->

* Version used:
* Browser name and version:
* Operating System and version (desktop or mobile):

## Possible Fix
<!-- Not obligatory, but suggest a fix or reason for the bug -->

