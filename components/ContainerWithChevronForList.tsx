import { useState } from "react";
import ChevronRight from "./svg/ChevronRight";

const ContainerWithChevronForList = ({
  label,
  children,
}: {
  label: string;
  children: React.ReactChild | React.ReactChild[];
}) => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <div
        className={`bg-gray-200 cursor-pointer ${
          open ? "rounded-t-lg" : "rounded-lg"
        } py-2 px-2`}
        onClick={(e) => {
          setOpen(!open);
        }}
      >
        <span className="text-xl">
          <span
            className={`inline-block w-4 h-4 mr-2 transform transition-transform ${
              open && " rotate-90"
            }`}
          >
            <ChevronRight />
          </span>
          {label}
        </span>
      </div>
      <div
        className={`transition-opacity duration-500 ease-in-out ${
          open ? "opacity-100 block" : "opacity-0 hidden"
        }`}
      >
        <div className="bg-gray-200 px-1 pb-1 rounded-b-lg">
          <div className="bg-white rounded-b-lg">{children}</div>
        </div>
      </div>
    </>
  );
};

export default ContainerWithChevronForList;
