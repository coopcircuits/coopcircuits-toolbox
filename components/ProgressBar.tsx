import React from "react";

const ProgressBar = ({
  progress,
  label,
}: {
  progress: number;
  label: string | React.ReactChild;
}) => (
  <div className="relative pt-1">
    <div className="flex mb-2 items-center justify-between">
      <div>
        <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-cc-orange-1000 bg-cc-orange-200">
          {label}
        </span>
      </div>
      <div className="text-right">
        <span className="text-xl font-thin inline-block text-gray-900">
          {Math.round(progress)}%
        </span>
      </div>
    </div>
    <div className="overflow-hidden h-3 mb-4 text-xs flex rounded-full bg-gray-200">
      <div
        style={{ width: `${progress}%` }}
        className="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-cc-orange"
      ></div>
    </div>
  </div>
);

export default ProgressBar;
