import classNames from "classnames";

export const getButtonClassNames = (
  state: "default" | "loading" | "disabled",
  size: "default" | "small"
) =>
  classNames({
    "inline-flex items-center border border-transparent text-white shadow":
      true,
    "text-2xl rounded-xl font-semibold shadow-lg px-10 py-4 leading-6 rounded-md":
      size == "default",
    "text-xs shadow px-2 py-1 leading-4 rounded": size == "small",
    "bg-cc-blue-600 cursor-not-allowed opacity-50": state === "disabled",
    "bg-cc-blue hover:bg-cc-blue-600 ": state == "default",
    "bg-cc-orange cursor-not-allowed ": state == "loading",
  });

const Button = ({
  label,
  state = "default",
  size = "default",
  onClick,
}: {
  label: string;
  state?: "default" | "loading";
  size?: "default" | "small";
  onClick: (e) => void;
}) => {
  return (
    <button
      className={getButtonClassNames(state, size)}
      onClick={(e) => state != "loading" && onClick(e)}
    >
      {state === "loading" && (
        <svg
          className="animate-spin -ml-1 mr-3 h-5 w-5 text-white float-left"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
        >
          <circle
            className="opacity-25"
            cx="12"
            cy="12"
            r="10"
            stroke="currentColor"
            strokeWidth="4"
          ></circle>
          <path
            className="opacity-75"
            fill="currentColor"
            d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
          ></path>
        </svg>
      )}
      {label}
    </button>
  );
};

export default Button;
