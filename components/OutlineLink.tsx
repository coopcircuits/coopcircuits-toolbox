import React from "react";

const OutlineLink = React.forwardRef(
  (
    {
      color,
      children,
      onClick,
      href,
    }: {
      color: "blue" | "orange";
      children: React.ReactChild;
      onClick?: (event: React.MouseEvent<HTMLAnchorElement>) => void;
      href?: string;
    },
    ref?: any
  ) => {
    return (
      <a
        className={`focus:shadow-outline focus:outline-none shadow border-4 rounded-lg  text-xl text-center  p-1 px-3 cursor-pointer font-semibold ${classesForColor(
          color
        )}`}
        href={href}
        onClick={onClick}
        ref={ref}
      >
        {children}
      </a>
    );
  }
);

const classesForColor = (color: "blue" | "orange") => {
  switch (color) {
    case "blue":
      return "border-cc-blue text-cc-blue";
    case "orange":
    default:
      return "border-cc-orange-300 text-cc-orange-300";
  }
};

export default OutlineLink;
