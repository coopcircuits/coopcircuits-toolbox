const Errors = ({
  type,
  objects,
}: {
  type: "product" | "order";
  objects: { id: string; name: string; error: string }[];
}) => {
  return (
    <div>
      <div className="text-3xl">Erreur</div>
      <div className="text-xl text-gray-700">
        {type === "product"
          ? "Les produits suivants  n’ont pas été transferés à Pastèque."
          : "Les commandes suivantes n’ont pas été transferées à Pastèque."}{" "}
        Cela peut être dû à un problème de connection.
        <br /> Veuillez{" "}
        <a href="mailto:support@coopcircuits.fr" className="text-cc-orange">
          contacter l’équipe support CoopCircuits
        </a>
        .
      </div>
      <div className="bg-gray-200 rounded-lg p-2 mt-5">
        <table className="table-fixed w-full bg-white">
          <thead className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
            <tr>
              <th className="w-1/2 py-2 px-1 text-left">
                {type === "product" ? "Produit" : "Commande"}
              </th>
              <th className="w-auto py-2 px-1 text-right">Raison</th>
              {/*  used to debug <th className="w-1/12 py-2 px-1 text-right">Réessayer</th> */}
            </tr>
          </thead>
          <tbody className="text-gray-600 text-sm font-light">
            {objects.map((o) => {
              return (
                <tr
                  key={o.id}
                  className="border-b border-gray-200 hover:bg-gray-100 hover:cursor-pointer"
                >
                  <td className="py-2 px-2">{o.name}</td>
                  <td className="py-2 px-2 text-red-400 text-right">
                    <pre>{o.error}</pre>
                  </td>
                  {/* used to debug <ProductRefresh p={p} /> */}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Errors;
