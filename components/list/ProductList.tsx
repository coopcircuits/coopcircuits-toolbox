import classNames from "classnames";
import React, { useState } from "react";

const ProductList = ({
  products,
  exportedProducts,
  loadingComponent,
  actionsComponent,
  errorComponent,
  errors,
}: {
  products: OFNMetaProduct[];
  exportedProducts?: number[];
  loadingComponent: React.ReactNode;
  actionsComponent: React.ReactNode;
  errorComponent: React.ReactNode;
  errors: ProductInError[];
}) => {
  return (
    <>
      <table className="table-fixed w-full">
        <thead>
          <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
            <th className="w-5 py-1 px-1">{actionsComponent}</th>
            <th className="w-1/3 py-3 px-6 text-left">Produit</th>
            <th className="py-3 px-6 text-left">Prix</th>
            <th className="py-3 px-6 text-left">Producteur</th>
            <th className="py-3 px-6 text-right">Statut Pasteque</th>
          </tr>
        </thead>
        <tbody className="text-gray-600 text-sm font-light">
          {!loadingComponent &&
            products &&
            products.map((product) => (
              <Product
                key={product.id}
                product={product}
                exportedProducts={exportedProducts}
                errors={errors}
              />
            ))}
        </tbody>
      </table>
      {loadingComponent}
      {errorComponent}
    </>
  );
};

const getTRClassNames = (isHighlighted: boolean) =>
  classNames({
    "border-b border-gray-200 hover:bg-gray-100 hover:cursor-pointer": true,
    "bg-green-200 transition duration-500 ease-in-out": isHighlighted,
  });
const getTRClassForVariants = (displayVariants: boolean) =>
  classNames({
    "border-b border-gray-200 hover:bg-gray-100": true,
    hidden: !displayVariants,
    "table-row": displayVariants,
  });
const getClassNameForArrow = (displayVariants: boolean) =>
  classNames({
    "h-4 w-4 transform transition-transform duration-500 ease-in-out object-right":
      true,
    "-rotate-90": !displayVariants,
    " ": displayVariants,
  });
const Product = ({
  product,
  exportedProducts,
  errors,
}: {
  product: OFNMetaProduct;
  exportedProducts?: number[];
  errors: ProductInError[];
}) => {
  const [displayVariants, setDisplayVariants] = useState(false);
  return (
    <>
      <tr
        className={getTRClassNames(false)}
        key={product.id}
        onClick={(e) => setDisplayVariants(!displayVariants)}
      >
        <td className="px-1">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className={getClassNameForArrow(displayVariants)}
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M19 13l-7 7-7-7m14-8l-7 7-7-7"
            />
          </svg>
        </td>
        <td className="py-2 px-6 text-left whitespace-nowrap">
          {product.name}
        </td>
        <td></td>
        <td className="py-2 px-6 text-left">
          {product.variants[0].producer_name}
        </td>
        <td className="py-2 px-6 text-right">
          {product.variants
            .map((v) => v.id)
            .every((id) => exportedProducts.includes(id))
            ? "✔️"
            : product.variants.every((v) => errors.find((e) => e.id === v.id))
            ? "✖️"
            : ""}
        </td>
      </tr>
      {product.variants.map((v) => (
        <tr className={getTRClassForVariants(displayVariants)} key={v.id}>
          <td className=""></td>
          <td className="py-2 px-6 text-left whitespace-nowrap">{v.name}</td>
          <td className="py-2 px-6 text-left">{v.price}€</td>
          <td></td>
          <td className="py-2 px-6 text-right">
            {exportedProducts.includes(v.id)
              ? "✔️"
              : errors.find((e) => e.id === v.id)
              ? "✖️"
              : ""}
          </td>
        </tr>
      ))}
    </>
  );
};

export default ProductList;
