const LoadingRows = ({ n }: { n: number }) => (
  <div className="py-2 mx-2 w-auto">
    <div className="animate-pulse flex-1 space-y-3 py-1">
      {[...Array(n)].map((e, i) => (
        <div className="h-6 bg-gray-300 rounded" key={i}></div>
      ))}
      {}
    </div>
  </div>
);

export default LoadingRows;
