import { currencyFormatter } from "../../lib/formatter";
import { ordersAreEquals } from "../../lib/OFNToPastequeBridge";

const OrderList = ({
  orders,
  errors,
  loadingComponent,
  actionsComponent,
}: {
  orders: Array<OFNDetailedOrder & { exported: boolean }>;
  errors: { order: PastequeOrder; error: Error }[];
  loadingComponent: React.ReactNode;
  actionsComponent: React.ReactNode;
}) => {
  return (
    <>
      <table className="table-fixed w-full">
        <thead>
          <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
            <th className="w-1 py-1 px-1">{actionsComponent}</th>
            <th className="w-7 py-3 px-1 text-left">id</th>
            <th className="w-7 py-3 px-1 text-left">Cycle de vente</th>
            <th className="w-20 py-3 px-1 text-left">Utilisateur·ice</th>
            <th className="w-10 py-3 px-1 text-left">Complétée le</th>
            <th className="w-9 py-3 px-1 text-right">Nombre de produits</th>
            <th className="w-5 py-3 px-1 text-right">Prix</th>
            <th className="w-1 py-3 px-1 text-right"></th>
          </tr>
        </thead>
        <tbody className="text-gray-600 text-sm font-light">
          {!loadingComponent &&
            orders &&
            orders.map((order) => (
              <Order
                key={order.id}
                order={order}
                error={
                  errors.find((o) => ordersAreEquals(order, o.order))?.error
                }
              />
            ))}
        </tbody>
      </table>
      {loadingComponent}
    </>
  );
};

const Order = ({
  order,
  error,
}: {
  order: OFNDetailedOrder & { exported: boolean };
  error: Error;
}) => {
  return (
    <tr className="text-gray-600 text-sm font-light">
      <td className="px-1"></td>
      <td className="py-2 px-1 text-left whitespace-nowrap">{order.number}</td>
      <td className="py-2 px-1 text-left">{order.order_cycle.id}</td>
      <td className="py-2 px-1 text-left">
        <Customer customer={order.customer} />
      </td>
      <td className="py-2 px-1 text-left">
        {new Date(order.completed_at).toLocaleDateString() === "Invalid Date"
          ? order.completed_at
          : new Date(order.completed_at).toLocaleDateString()}
      </td>
      <td className="py-2 px-1 text-right">{order.line_items.length}</td>
      <td className="py-2 px-1 text-right">{currencyFormatter(order.total)}</td>
      <td className="py-2 px-6 text-right">
        {order.exported ? "✔️" : error ? "✘" : ""}
      </td>
    </tr>
  );
};

const Customer = ({ customer }: { customer: OFNCustomer }) => {
  return (
    <div>
      {`${customer.first_name + " " + customer.last_name} // ${customer.email}`}
      <span>
        {customer.tags.map((tag) => (
          <span
            key={tag}
            className="ml-4 text-xs inline-flex items-center font-bold leading-sm uppercase px-3 py-1 rounded-full bg-white text-gray-500 border"
          >
            {tag}
          </span>
        ))}
      </span>
    </div>
  );
};

export default OrderList;
