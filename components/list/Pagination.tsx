import classNames from "classnames";

const getClassForNav = (active: boolean, current: boolean) =>
  classNames({
    "relative inline-flex py-2 border border-gray-300 bg-white text-sm font-medium":
      true,
    "text-gray-500": !active && !current,
    "text-gray-700 hover:cursor-pointer hover:bg-gray-50": active && !current,
    "text-gray-900 bg-gray-200": current,
  });

const Pagination = ({
  page,
  per_page,
  results,
  pages,
  onClick,
  children,
}: {
  page: number;
  per_page: number;
  results: number;
  pages: number;
  onClick: (page: number) => void;
  children?: JSX.Element;
}) => (
  <div className="bg-white py-3 border-t border-gray-200">
    <div className="flex items-center justify-around flex-grow-0">
      <div className="w-1/3">{children}</div>
      <div className="w-1/3 text-center">
        <p className="text-sm text-gray-700">
          Résultats de{" "}
          <span className="font-medium">{(page - 1) * per_page + 1}</span> à{" "}
          <span className="font-medium">
            {Math.min(page * per_page, results)}
          </span>{" "}
          sur <span className="font-medium">{results}</span> résultats
        </p>
      </div>
      <div className="w-1/3">
        <nav
          className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px cursor-pointer float-right"
          aria-label="Pagination"
        >
          <a
            className={`${getClassForNav(page > 1, false)} px-2 rounded-l-md`}
            onClick={(e) => page > 1 && onClick(page - 1)}
          >
            <span className="sr-only">Previous</span>
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </a>
          <a
            onClick={(e) => page !== 1 && onClick(1)}
            className={`${getClassForNav(page !== 1, page === 1)}  px-4`}
          >
            {1}
          </a>

          <span
            className={`${getClassForNav(
              false,
              page !== 1 && page !== pages
            )} px-4 `}
          >
            {page == 1 || page === pages ? "..." : page}
          </span>

          <a
            onClick={(e) => page !== pages && onClick(pages)}
            className={`${getClassForNav(page !== pages, page === pages)} px-4`}
          >
            {pages}
          </a>
          <a
            onClick={(e) => page < pages && onClick(page + 1)}
            className={`${getClassForNav(
              page < pages,
              false
            )} px-2 rounded-r-md `}
          >
            <span className="sr-only">Next</span>
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </a>
        </nav>
      </div>
    </div>
  </div>
);
export default Pagination;
