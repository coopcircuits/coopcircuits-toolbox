const ErrorComponent = ({ message }: { message: string }) => (
  <div
    className="text font-semibold text-left text-cc-orange-1000 bg-cc-orange-200 border border-cc-orange h-12 flex items-center px-6 py-8 rounded"
    role="alert"
  >
    {message}
  </div>
);

export default ErrorComponent;
