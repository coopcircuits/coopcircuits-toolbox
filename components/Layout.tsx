import Link from "next/link";
import Head from "next/head";
import { ReactNode } from "react";
import Image from "next/image";
import User from "../containers/pasteque/User";

export default function Layout({
  pathname,
  children,
}: {
  pathname: string;
  children: ReactNode;
}) {
  const title = "Boîte à outils CoopCircuits";
  return (
    <>
      <Head>
        <title>{title}</title>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/favico/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favico/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favico/favicon-16x16.png"
        />
      </Head>
      <div className="flex flex-col min-h-screen">
        <header>
          <nav className="font-sans flex flex-row text-left justify-between py-4 px-6 bg-white shadow w-full">
            <div>
              <Link href="/">
                <span className="cursor-pointer flex">
                  <span className="absolute top-4 inline-block mr-2 align-bottom">
                    <Image
                      src="/coopcircuits.png"
                      layout="fixed"
                      width={35}
                      height={35}
                      className="object-cover object-right"
                    />
                  </span>
                  <a className="text-2xl no-underline text-grey-darkest hover:text-blue-dark ml-10">
                    {title}
                  </a>
                </span>
              </Link>
            </div>
            <div>
              <User />
            </div>
          </nav>
        </header>
        <main className="container mx-auto flex-grow">{children}</main>
        <footer className="h-auto bg-cc-gray flex flex-row items-center justify-center ">
          <div className="mx-2 lg:mx-10 lg:my-10">
            <Image src="/white-logo.png" width={60} height={60} />
          </div>
          <div className="w-4/5 m-2 md:m-4 md:w-2/3 lg:w-1/2 xl:w-1/3 ">
            <p className="text-gray-400 text-xs text-justify">
              Lire{" "}
              <a
                className="hover:text-white text-gray-300"
                href="https://apropos.coopcircuits.fr/informations-legales/"
                target="_blank"
              >
                nos conditions générales d'utilisation
              </a>{" "}
              | Nous trouver sur{" "}
              <a
                className="hover:text-white text-gray-300"
                href="https://gitlab.com/coopcircuits/coopcircuits-toolbox"
                target="blank"
              >
                GitLab
              </a>{" "}
              | Contacter l’équipe de{" "}
              <a
                className="hover:text-white text-gray-300"
                href="mailto:support@coopcircuits.fr"
                target="blank"
              >
                support CoopCircuits
              </a>{" "}
              | Le code de cette boîte à outils est protégé sous licence{" "}
              <a
                className="hover:text-white text-gray-300"
                href="https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)"
                target="blank"
              >
                AGPL 3
              </a>
              . Les autres productions disponibles sur ce site sont protégées
              sous licence{" "}
              <a
                className="hover:text-white text-gray-300"
                href="https://creativecommons.org/licenses/by-sa/3.0/"
                target="blank"
              >
                CC BY-SA 3.0
              </a>
              . | Nous prenons soin de vos données. Voir notre{" "}
              <a
                className="hover:text-white text-gray-300"
                href="https://nextcloud.coopcircuits.fr/index.php/s/SxnPKCZiiJG3b5b"
              >
                politique de gestion des données{" "}
              </a>
              et{" "}
              <a
                className="hover:text-white text-gray-300"
                href="https://www.coopcircuits.fr/#/policies/cookies"
                target="blank"
              >
                politique de cookies
              </a>
              .
            </p>
          </div>
        </footer>
      </div>
    </>
  );
}
