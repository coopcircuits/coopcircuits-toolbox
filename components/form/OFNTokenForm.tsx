import { useState } from "react";
import { getButtonClassNames } from "../Button";

const OFNTokenForm = ({ onSubmit }: { onSubmit: (token: string) => void }) => {
  const [token, setToken] = useState("");
  const canSubmit = () => {
    return token.length > 0;
  };
  return (
    <>
      <div className="text-4xl mb-4">Clef d'API CoopCircuits</div>
      <div className="grid grid-cols-1 gap-6">
        <input
          type="text"
          placeholder="Token de l'API OFN"
          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
          value={token}
          onChange={(e) => setToken(e.target.value)}
        />
      </div>
      <div className="mt-5">
        <button
          type="submit"
          className={getButtonClassNames(
            canSubmit() ? "default" : "disabled",
            "default"
          )}
          onClick={(e) => onSubmit(token)}
          disabled={!canSubmit()}
        >
          OK
        </button>
      </div>
    </>
  );
};

export default OFNTokenForm;
