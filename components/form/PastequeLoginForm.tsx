import { useState } from "react";
import Button, { getButtonClassNames } from "../Button";
import Error from "../list/ErrorComponent";

const PastequeLoginForm = ({
  onSubmit,
  error,
}: {
  error: string | null;
  onSubmit: (login: string, password: string) => any;
}) => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [submitting, setSubmitting] = useState(false);

  const canSubmit = () => {
    return login.length > 0 && password.length > 0 && !submitting;
  };

  const submit = async (event) => {
    event.preventDefault();
    if (submitting) {
      return;
    }
    setSubmitting(true);
    await onSubmit(login, password);
    setSubmitting(false);
  };

  return (
    <>
      <div className="text-4xl mb-4">Pastèque</div>
      <form onSubmit={submit}>
        <div className="grid grid-cols-1 gap-6">
          <input
            type="text"
            placeholder="Nom d'utilisateur"
            className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
            autoComplete="off"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setLogin(e.target.value)
            }
          />
          <input
            type="password"
            placeholder="Mot de passe"
            className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
            autoComplete="off"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setPassword(e.target.value)
            }
          />
        </div>
        <div className="mt-5">
          <button
            type="submit"
            className={getButtonClassNames(
              canSubmit() ? "default" : "disabled",
              "default"
            )}
            disabled={!canSubmit()}
          >
            Se connecter
          </button>
        </div>
        {error && (
          <div className="mt-5 ">
            <Error message={error} />
          </div>
        )}
      </form>
    </>
  );
};

export default PastequeLoginForm;
