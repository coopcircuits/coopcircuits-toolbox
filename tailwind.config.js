module.exports = {
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./containers/**/*.{js,ts,jsx,tsx}",
  ],
  // mode: 'jit',
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "cc-gray": {
          DEFAULT: "#3B3B3B",
        },
        "cc-orange": {
          1000: "#C45A3C",
          DEFAULT: "#F5714B",
          300: "#F68260",
          200: "#FCD4C9",
        },
        "cc-blue": {
          DEFAULT: "#0096AD",
          600: "#33ABBD",
          300: "#99D5DE",
          200: "#B2DFE6",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms")],
};
