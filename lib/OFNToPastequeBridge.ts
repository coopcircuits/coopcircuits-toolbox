import PastequeApiClient from "./pasteque-api";
import { getTax, toWithoutTaxPrice } from "./ofn-api/tax";
import { createAsyncAction, successResult } from "pullstate";
import slugify from "slugify";
import memoize from "lodash.memoize";

export const asyncActionGetAllTaxes = createAsyncAction(
  async ({ pastequeToken }) => {
    const apiClient = new PastequeApiClient(pastequeToken);
    const taxes = await apiClient.getAllTaxes();
    return successResult({ taxes: taxes });
  }
);

const createPastequeIdAndReference = (
  ref: string
): { id: PastequeId; reference: string } => ({
  id: {
    id: ref,
    source: "coopcircuits.fr",
  },
  reference: ref,
});

const createPastequeIdAndReferenceForOrder = (
  id: string,
  reference: string
) => ({
  ...createPastequeIdAndReference(id),
  reference: reference,
});

const mapper = (
  pastequeTaxes: PastequeTax[],
  category: PastequeCategory,
  p: OFNProduct
): ExtandedPastequeProduct => {
  const tax = getTax(p.tax_category_id, pastequeTaxes);
  return {
    ...createPastequeIdAndReference(p.id.toString()),
    label: p.name,
    category: category.id,
    priceSell: toWithoutTaxPrice(+p.price, tax),
    tax: tax.id,
    visible: true,
  };
};

export const categoryMapper = (
  categoryLabel: string
): PastequeCategoryUnsaved => ({
  label: categoryLabel,
  ...createPastequeIdAndReference(slugify(categoryLabel, { lower: true })),
});

export const exportProductToPasteque = async ({
  pastequeToken,
  product,
  pastequeTaxes,
  pastequeCategory,
}: {
  pastequeToken: string;
  product: OFNProduct;
  pastequeTaxes: PastequeTax[];
  pastequeCategory: PastequeCategory;
}): Promise<ExtandedPastequeProduct> => {
  const apiClient = new PastequeApiClient(pastequeToken);

  // Map OFN products to Pasteque products
  const pastequeProductToSynchronize: ExtandedPastequeProduct = mapper(
    pastequeTaxes,
    pastequeCategory,
    product
  );

  // Update or create?
  return await apiClient.createOrUpdateProduct(pastequeProductToSynchronize);
};

// Orders
export const orderMapper = (
  o: OFNDetailedOrder,
  pastequeTaxes: PastequeTax[]
): PastequeOrder => {
  const prducts = o.line_items.map((l) => lineMapper(l, pastequeTaxes));
  const adjustements = o.adjustments.map((a) =>
    adjustmentMapper(a, pastequeTaxes)
  );
  const pastequeOrder: PastequeOrder = {
    ...createPastequeIdAndReferenceForOrder(o.id.toString(), o.number),
    date: parseDate(o.completed_at)?.getTime() / 1000,
    customerName: `${o.full_name} // ${o.phone.toString()}`,
    lines: [...prducts, ...adjustements].filter((e) => e !== null),
    userName: "", // TODO: understand this field
    tags: [{ group: "searchkey", value: o.order_cycle.id.toString() }],
    discountProfile: o.customer ? getDiscountProfile(o.customer.tags) : null,
  };
  return pastequeOrder;
};

export const getDiscountProfile = memoize((tags: string[]): number | null => {
  if (tags === null || tags.length === 0) {
    return null;
  }
  const tagsToDiscountProfilesPairing = getTagsToDiscountProfilesPairing();
  if (tagsToDiscountProfilesPairing.length === 0) {
    return null;
  }
  const matchingTags = tagsToDiscountProfilesPairing.filter((pairing) =>
    tags.includes(pairing.split("=")[0])
  );
  if (matchingTags.length === 0) {
    return null;
  }
  // Get the first matching tags and return the associated discount profile
  return +matchingTags[0].split("=")[1];
});

export const getTagsToDiscountProfilesPairing = memoize((): Array<string> => {
  if (!process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS) {
    return [];
  }
  return (
    process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS.split(",") || []
  );
});

const frToEn = {
  janvier: "january",
  février: "february",
  mars: "march",
  avril: "april",
  mai: "may",
  juin: "june",
  juillet: "july",
  août: "august",
  septembre: "september",
  octobre: "october",
  novembre: "november",
  décembre: "december",
};

export const parseDate = (date: string): Date | null => {
  if (!date || !date.length) {
    return null;
  }
  const d = new Date(
    date
      .split(" ")
      .map((s) => frToEn[s] || s)
      .join(" ") + " UTC"
  );
  return isNaN(d.getTime()) ? null : d;
};

export const ordersAreEquals = (
  ofnOrder: OFNDetailedOrder,
  pastequeOrder: PastequeOrder
) => {
  return ofnOrder.id.toString() === pastequeOrder.id.id;
};

const lineMapper = (
  line: OFNProductInOrder,
  pastequeTaxes: PastequeTax[]
): PastequeLine => {
  const tax = getTax(line.tax_category_id, pastequeTaxes);
  return {
    productLabel: line.variant.name_to_display,
    customUnitPrice: toWithoutTaxPrice(+parseFloat(line.price), tax),
    quantity: line.quantity,
    product: null,
    customTax: tax.id,
  };
};

export const adjustmentMapper = (
  adjustement: OFNAdjustment,
  pastequeTaxes: PastequeTax[]
): PastequeLine => {
  const amount = +parseFloat(adjustement.amount);
  if (amount === 0 || adjustement.tax_category_id === null) {
    return null;
  }
  const tax = getTax(adjustement.tax_category_id, pastequeTaxes);
  return {
    productLabel: adjustement.label,
    customUnitPrice: toWithoutTaxPrice(amount, tax),
    quantity: 1,
    product: null,
    customTax: tax.id,
  };
};

export const isExported = (
  order: OFNDetailedOrder,
  exported: PastequeOrder[]
): boolean => {
  return exported.some((e) => ordersAreEquals(order, e));
};
