class LocalStorage {
  set(key: LocalStorageKey, value: string): void {
    if (typeof window !== "undefined") {
      window.localStorage.setItem(key, value);
    }
  }
  get(key: LocalStorageKey): string | null {
    if (typeof window !== "undefined") {
      return window.localStorage.getItem(key);
    }
    return null;
  }
  delete(key: LocalStorageKey): void {
    if (typeof window !== "undefined") {
      window.localStorage.removeItem(key);
    }
  }
}

type LocalStorageKey =
  | "pasteque-login"
  | "pasteque-password"
  | "pasteque-token"
  | "ofn-token";
export default new LocalStorage();
