type PastequeStore = {
  pasteque: {
    isLoggedIn: "unknown" | "loggedIn" | "loggedOut";
    form: {
      error: string;
    };
    token: string | null;
  };
  ofn: {
    token: string | null;
    showLogginForm: boolean;
  };
  paginatedProducts: Paginated<OFNMetaProduct>;
  productsList: {
    page: number;
  };
  ordersList: {
    page: number;
  };
  import: {
    orders: {
      data: Paginated<OFNDetailedOrder>;
      pagination: OFNPagination;
      state: "pending" | "running" | "completed";
    };
    pagination: OFNPagination;
    loading: "false" | "all" | "errors";
    pagesInError: PagesInError[];
  };
  exportProducts: {
    status: "pending" | "completed";
    products: number[];
    variantsCount: number;
    productsInError: ProductInError[];
  };
  exportOrders: {
    status: "running" | "completed";
    label: string;
    exportedOrders: PastequeOrder[];
    ordersInError: { order: PastequeOrder; error: Error }[];
  };
};

type ProductInError = {
  id: number;
  product: OFNProduct;
  token: string;
  pastequeTaxes: PastequeTax[];
  pastequeCategory: PastequeCategory;
  error: string;
};

type PagesInError = {
  error: string;
  args: {
    token: string;
    per_page: number;
    page: number;
  };
};

type Paginated<T> = {
  [page: number]: T[];
};

type OFNTax = {
  id: number;
  rate: number;
};

type OFNShop = {
  active: boolean;
  description: string;
  hash: string;
  id: number;
  logo: string;
  long_description: string;
  name: string;
  orders_close_at: string;
  path: string;
  website: string;
};

type OFNMetaProduct = {
  id: number;
  name: string;
  sku: string;
  variant_unit: "items" | "weight";
  variant_unit_scale: number | null;
  variant_unit_name: string;
  inherits_properties: boolean;
  on_hand: number;
  price: string;
  available_on: string;
  permalink_live: string;
  image_url: string;
  thumb_url: string;
  producer_id: number;
  category_id: number;
  variants: OFNProduct[];
};

type OFNProduct = {
  id: OFNProductId;
  name: string;
  producer_name: string;
  image: string;
  sku: string;
  options_text: string;
  unit_value: number;
  unit_description: string;
  unit_to_display: string;
  display_as: string | null;
  display_name: string | null;
  name_to_display: string;
  variant_overrides_count: number;
  price: string;
  on_demand: boolean;
  on_hand: number;
  in_stock: boolean;
  stock_location_id: number;
  stock_location_name: string;
  tax_category_id: number;
};

type OFNProductId = number;

type OFNOrder = {
  id: number;
  number: string;
  user_id: number;
  customer_id: number;
  full_name: string;
  email: string;
  phone: string;
  completed_at: string;
  display_total: string;
  edit_path: string;
  state: string;
  payment_state: string;
  shipment_state: string;
  payments_path: string;
  ready_to_ship: boolean;
  ready_to_capture: boolean;
  created_at: string;
  distributor_name: string;
  special_instructions: string;
  display_outstanding_balance: string;
  item_total: string;
  adjustment_total: string;
  payment_total: string;
  total: string;
  distributor: {
    id: number;
  };
  order_cycle: {
    id: number;
  };
};

type OFNAdjustment = {
  id: number;
  amount: string;
  label: string;
  eligible: boolean;
  adjustable_type: string;
  adjustable_id: number;
  originator_type: string;
  originator_id: number;
  tax_category_id: ?number;
};

type OFNDetailedOrder = OFNOrder & {
  adjustments: OFNAdjustment[];
  shipping_method: {
    id: number;
    require_ship_address: boolean;
    name: string;
    description: string;
    price: string;
  };
  ship_address: OFNAddress;
  bill_address: OFNAddress;
  line_items: OFNProductInOrder[];
  payments: [
    {
      amount: string;
      updated_at: sting;
      payment_method: string;
      state: "checkout" | string;
      cvv_response_message: ?string;
    }
  ];
  customer: OFNCustomer;
};

type OFNProductInOrder = {
  id: number;
  quantity: number;
  max_quantity: number;
  tax_category_id: number;
  price: string;
  order_id: number;
  variant: {
    id: number;
    product_name: string;
    sku: string;
    options_text: string;
    unit_value: mber;
    unit_description: string;
    unit_to_display: string;
    display_as: string;
    display_name: string;
    name_to_display: string;
    price: string;
    on_demand: boolean;
    on_hand: number;
    fees: any;
    price_with_fees: string;
    tag_list: any[];
    thumb_url: string;
    unit_price_price: string;
    unit_price_unit: string;
  };
};

type OFNAddress = {
  id: number;
  zipcode: string;
  city: string;
  state_name: string;
  state_id: number;
  phone: string;
  firstname: string;
  lastname: string;
  address1: string;
  address2: string;
  country_id: number;
  country_name: string;
};

type OFNAddressV1 = {
  phone: string;
  latitude: number | null;
  longitude: number | null;
  first_name: string;
  last_name: string;
  street_address_1: string;
  street_address_2: string;
  postal_code: string;
  locality: string;
  region: string;
  country: strign;
};

type OFNPagination = {
  results: number;
  pages: number;
  page: number;
  per_page: number;
};

type OFNCustomer = {
  id: number;
  enterprise_id: number;
  first_name: string;
  last_name: string;
  code: string | null;
  email: string;
  allow_charges: boolean;
  terms_and_conditions_accepted_at: string | null;
  tags: string[];
  billing_address: OFNAddressV1;
  shipping_address: OFNAddressV1;
};

type PastequeProduct = {
  label: string;
  priceSell: number;
  category: PastequeCategoryId;
  tax: PastequeTaxId;
  source?: string;
};

type PastequeId = {
  id: string;
  source: string;
};

type ExtandedPastequeProduct = PastequeProduct & {
  id: PastequeId;
  reference?: string;
  visible: boolean;
};

type PastequeTax = {
  id: PastequeTaxId;
  label: string;
  rate: number;
};
type PastequeTaxId = number;

type PastequeCategoryUnsaved = {
  reference: string;
  label: string;
  id: PastequeId;
};

type PastequeCategory = PastequeCategoryUnsaved & {
  id: PastequeCategoryId;
  dispOrder: number;
  parent: PastequeCategoryId | null;
};

type PastequeCategoryId = number;

type PastequeOrder = {
  id: PastequeId;
  reference: string;
  date?: number;
  endDate?: string;
  customerName: string;
  customer?: number;
  custCount?: number;
  userName: string;
  user?: ?number;
  discountRate?: number;
  tariffArea?: ?number;
  visible?: boolean;
  lines: PastequeLine[];
  tags: {
    group: string;
    value: string;
  }[];
  discountProfile: ?number;
};

type PastequeLine = {
  dispOrder?: number;
  productLabel: string;
  customUnitPrice: ?number;
  quantity: number;
  discountRate?: ?number;
  product: ?number;
  customTax: PastequeTaxId;
};
