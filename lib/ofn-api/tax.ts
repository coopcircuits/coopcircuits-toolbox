const taxes: OFNTax[] = [
  {
    id: null,
    rate: 0,
  },
  {
    id: 2,
    rate: 0.2,
  },
  {
    id: 3,
    rate: 0.1,
  },
  {
    id: 5,
    rate: 0.021,
  },
  {
    id: 6,
    rate: 0,
  },
  {
    id: 4,
    rate: 0.055,
  },
  {
    id: 7,
    rate: 0,
  },
  {
    id: 8,
    rate: 0.214,
  },
];

export const getTax = (
  tax_category_id: number | null,
  availableTaxes: PastequeTax[]
): PastequeTax => {
  const ofnTax = taxes.find((t) => t.id === tax_category_id);
  return availableTaxes.find((t) => t.rate === ofnTax.rate);
};

export const toWithoutTaxPrice = (price: number, tax: PastequeTax): number => {
  const ret = price / (1 + tax.rate);
  return Math.round(ret * 1e6) / 1e6;
};
