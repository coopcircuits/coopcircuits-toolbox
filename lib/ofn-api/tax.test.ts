import { taxes } from "../mocks/pasteque";
import { getTax, toWithoutTaxPrice } from "./tax";

describe("Test the OFN to pastèque taxes system", () => {
  it("get the right Pasteque tax", () => {
    expect(getTax(null, taxes)).toEqual(taxes[0]);
    expect(getTax(2, taxes).id).toBe(4);
    expect(getTax(3, taxes).id).toBe(2);
    expect(getTax(5, taxes).id).toBe(3);
    expect(getTax(6, taxes).id).toBe(1);
    expect(getTax(4, taxes).id).toBe(5);
    expect(getTax(7, taxes).id).toBe(1);
    expect(getTax(8, taxes).id).toBe(7);
  });

  it("return the right HT values", () => {
    const tax20: PastequeTax = {
      id: 1,
      label: "default",
      rate: 0.2,
    };
    expect(toWithoutTaxPrice(100, tax20)).toBe(83.333333);
    expect(toWithoutTaxPrice(3.7, tax20)).toBe(3.083333);

    const tax55: PastequeTax = {
      id: 1,
      label: "default",
      rate: 0.055,
    };
    expect(toWithoutTaxPrice(4.22, tax55)).toBe(4);
    expect(toWithoutTaxPrice(0.86, tax55)).toBe(0.815166);

    const taxNull: PastequeTax = {
      id: 1,
      label: "Sans TVA",
      rate: 0,
    };
    expect(toWithoutTaxPrice(100, taxNull)).toBe(100);
  });
});

export {};
