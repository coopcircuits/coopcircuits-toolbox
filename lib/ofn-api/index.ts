import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import dateFormat from "dateformat";
import curlirize from "axios-curlirize";
import PQueue from "p-queue";

export const HEADER_API_VERSION = "x-api-version";

const createClient = (baseURL: string) =>
  axios.create({
    baseURL: baseURL,
    timeout: 10000,
    headers: {
      [HEADER_API_VERSION]: "v0",
    },
  });

class ApiClient {
  client: any;
  token: string;
  static format: string = "yyyy-mm-dd HH:MM:ss";
  constructor(baseURL: string, token: string) {
    this.client = createClient(baseURL);
    // curlirize(this.client);
    this.token = token;

    // Add request interceptor
    this.client.interceptors.request.use((config: AxiosRequestConfig) => {
      if (this.token) {
        config.params = { ...config.params, token: this.token };
      }
      return config;
    });

    // Add a response interceptor
    this.client.interceptors.response.use(
      (response: AxiosResponse) => {
        return response;
      },
      (error: AxiosError) => {
        if (error.response) {
          throw error.response;
        }
        throw new Error(error as unknown as string);
      }
    );
  }

  async getProducts(
    page: number = 0,
    per_page: number = 15
  ): Promise<{ products: OFNMetaProduct[]; pagination: OFNPagination }> {
    const resp = await this.client.get("products/bulk_products", {
      params: { page: page, per_page: per_page },
    });
    return { products: resp.data.products, pagination: resp.data.pagination };
  }

  async getOrders(
    page: number,
    per_page: number = 15
  ): Promise<{
    orders: OFNDetailedOrder[];
    pagination: OFNPagination;
  }> {
    const today = new Date();
    const start = new Date(new Date().setDate(today.getDate() - 10));
    today.setHours(23, 59, 59, 99);
    start.setHours(0, 0, 0, 0);

    const resp = await this.client.get("orders", {
      params: {
        "q[payment_state_eq]": "balance_due",
        "q[state_eq]": "complete",
        "q[s]": "completed_at desc",
        "q[completed_at_gteq]": dateFormat(start, ApiClient.format),
        "q[completed_at_lteq]": dateFormat(today, ApiClient.format),
        page: page,
        per_page: per_page,
      },
    });
    const detailedOrders: OFNDetailedOrder[] = [];

    const queue = new PQueue({ concurrency: 5 });
    resp.data.orders.forEach(async (order: OFNOrder) => {
      // Retrieve detailed order
      await queue.add(async () => {
        const resp = await this.client.get(`orders/${order.number}.json`);
        const customer = await this.getCustomer(resp.data.customer_id);
        detailedOrders.push({
          ...resp.data,
          customer: customer,
        });
      });
    });
    await queue.onIdle();
    return { orders: detailedOrders, pagination: resp.data.pagination };
  }

  async getShop(id: number): Promise<OFNShop> {
    const resp = await this.client.get(`shops/${id}.json`);
    return resp.data;
  }

  async getCustomer(id: number): Promise<OFNCustomer> {
    const resp = await this.client.get(`customers/${id}`, {
      headers: {
        [HEADER_API_VERSION]: "v1",
      },
    });
    return resp.data.data.attributes;
  }
}

export default ApiClient;
