import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

const source_name = "coopcircuits.fr";

const createClient = (baseURL: string) =>
  axios.create({
    baseURL: baseURL,
    timeout: 2000,
  });
export const pastequeAPIURL = `${process.env.NEXT_PUBLIC_PASTEQUE_HOST}/api`;
class ApiClient {
  client: any;
  token: string;
  constructor(token?: string) {
    this.client = createClient(pastequeAPIURL);
    this.token = token;

    // Add request interceptor
    this.client.interceptors.request.use((config: AxiosRequestConfig) => {
      if (this.token) {
        config.headers = { ...config.headers, Token: this.token };
      }
      return config;
    });

    // Add a response interceptor
    this.client.interceptors.response.use(
      (response: AxiosResponse) => {
        this.token = response.headers.token;
        return response;
      },
      (error: AxiosError) => {
        throw new Error(error as unknown as string);
      }
    );
  }

  async login({ login, password }: { login: string; password: string }) {
    const resp = await this.client.post("login", {
      user: login,
      password,
    });
    return resp.headers.token;
  }

  async getAllTaxes(): Promise<PastequeTax[]> {
    const request: AxiosRequestConfig = {
      method: "GET",
      url: "tax/getAll",
    };
    const resp = await this.client(request);
    return resp.data;
  }

  async getAllCategories(): Promise<PastequeCategory[]> {
    const request: AxiosRequestConfig = {
      method: "GET",
      url: "category/getAll",
    };
    const resp = await this.client(request);
    return resp.data;
  }

  async getAllProducts(): Promise<ExtandedPastequeProduct[]> {
    const request: AxiosRequestConfig = {
      method: "GET",
      url: "product/getAll",
      timeout: 5000,
    };
    const resp = await this.client(request);
    return resp.data;
  }

  async createCategory(
    category: PastequeCategoryUnsaved
  ): Promise<PastequeCategory> {
    const request: AxiosRequestConfig = {
      method: "POST",
      url: "category",
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        ...category,
      },
    };
    const response = await this.client(request);
    return response.data;
  }

  async archiveProduct(p: ExtandedPastequeProduct) {
    const product = {
      ...p,
      visible: false,
    };
    return this.createOrUpdateProduct(product);
  }

  async createOrUpdateProduct(
    product: ExtandedPastequeProduct
  ): Promise<ExtandedPastequeProduct> {
    // Fill source and id
    const request: AxiosRequestConfig = {
      method: "POST",
      url: "product",
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        ...product,
      },
    };
    const response = await this.client(request);
    return response.data;
  }

  async createOrUpdateOrder(order: PastequeOrder): Promise<PastequeOrder> {
    const request: AxiosRequestConfig = {
      method: "POST",
      url: "extorder",
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        ...order,
      },
    };
    const response = await this.client(request);
    return response.data;
  }
}

export default ApiClient;
