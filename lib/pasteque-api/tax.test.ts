import { taxes } from "../mocks/pasteque";
import { get5_5Tax } from "./tax";

describe("Pasteque API Tax, test get5_5Tax()", () => {
  it("should return the tax with the 5.5% rate", () => {
    expect(get5_5Tax(taxes)).toEqual(taxes[4]);
  });
});

export default {};
