const get5_5Tax = (taxes: PastequeTax[]) => {
  const tax = taxes.find((tax) => tax.rate === 0.055);
  return tax;
};

export { get5_5Tax };
