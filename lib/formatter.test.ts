import { currencyFormatter } from "./formatter";

describe("test currencyFormatter()", () => {
  it("handle default cases", () => {
    expect(currencyFormatter(2)).toBe("2,00\xa0€");
    expect(currencyFormatter(2.01)).toBe("2,01\xa0€");
    expect(currencyFormatter("2")).toBe("2,00\xa0€");
    expect(currencyFormatter("2.01")).toBe("2,01\xa0€");
  });
  it("handle special cases", () => {
    expect(currencyFormatter(undefined)).toBe(null);
    expect(currencyFormatter(null)).toBe(null);
    expect(currencyFormatter("")).toBe(null);
    expect(currencyFormatter("a")).toBe(null);
  });
});

export default {};
