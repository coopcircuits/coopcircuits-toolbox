export const currencyFormatter = (price: string | number) => {
  if (price && !isNaN(price as number)) {
    return new Intl.NumberFormat("fr-FR", {
      style: "currency",
      currency: "EUR",
    }).format(parseFloat(price as string));
  }
  return null;
};
