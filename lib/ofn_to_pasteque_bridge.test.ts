import memoize from "lodash.memoize";
import { order as ofnOrder } from "./mocks/ofn";
import { order as pastequeOrder, taxes } from "./mocks/pasteque";
import {
  isExported,
  orderMapper,
  ordersAreEquals,
  parseDate,
  adjustmentMapper,
  getDiscountProfile,
  getTagsToDiscountProfilesPairing,
} from "./OFNToPastequeBridge";

const buildAdjustment = (args: any): OFNAdjustment => ({
  ...args,
  id: null,
  eligible: true,
  adjustable_type: null,
  adjustable_id: null,
  originator_type: null,
  originator_id: null,
});

describe("test adjustmentMapper()", () => {
  it("handle the default case with tax", () => {
    const adjustment = {
      amount: "2.0",
      label: "Frais de transaction",
      tax_category_id: 5,
    };
    const line: PastequeLine = {
      productLabel: "Frais de transaction",
      customUnitPrice: 1.958864,
      quantity: 1,
      customTax: 3,
      product: null,
    };
    expect(adjustmentMapper(buildAdjustment(adjustment), taxes)).toEqual(line);
  });
  it("handle the case with a null amount", () => {
    const adjustment = {
      amount: "0.0",
      label: "Frais de transaction",
      tax_category_id: null,
    };

    expect(adjustmentMapper(buildAdjustment(adjustment), taxes)).toEqual(null);
  });
  it("handle the case with a negative amount", () => {
    const adjustment = {
      amount: "-2.0",
      label: "Remboursement",
      tax_category_id: 4,
    };
    const line: PastequeLine = {
      productLabel: "Remboursement",
      customUnitPrice: -1.895735,
      quantity: 1,
      customTax: 5,
      product: null,
    };
    expect(adjustmentMapper(buildAdjustment(adjustment), taxes)).toEqual(line);
  });
  it("handle the case with an amount but no tax_category_id", () => {
    const adjustment = {
      amount: "2.0",
      label: "Shipment",
      tax_category_id: null,
    };
    // Ignore it if no tax_category_id
    expect(adjustmentMapper(buildAdjustment(adjustment), taxes)).toEqual(null);
  });
});

describe("test orderMapper()", () => {
  afterEach(() => {
    delete process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS;
    // clear the memoized function cache
    getTagsToDiscountProfilesPairing.cache = new memoize.Cache();
    getDiscountProfile.cache = new memoize.Cache();
  });
  it("handle the default case", () => {
    expect(orderMapper(ofnOrder, taxes)).toEqual(pastequeOrder);
  });
  it("handle when there is a tags/discountProfile pairing", () => {
    process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS = "tarif=1";

    expect(orderMapper(ofnOrder, taxes)).toEqual({
      ...pastequeOrder,
      discountProfile: 1,
    });
  });
});

describe("test isExported()", () => {
  it("handle the default case", () => {
    expect(isExported(ofnOrder, [pastequeOrder])).toBe(true);
    expect(isExported({ ...ofnOrder, id: 1 }, [pastequeOrder])).toBe(false);
  });
});

describe("test ordersAreEquals()", () => {
  it("handle the default case", () => {
    expect(ordersAreEquals(ofnOrder, pastequeOrder)).toBe(true);
    expect(ordersAreEquals({ ...ofnOrder, id: 1 }, pastequeOrder)).toBe(false);
    expect(
      ordersAreEquals(ofnOrder, {
        ...pastequeOrder,
        id: { id: "1", source: "coucou" },
      })
    ).toBe(false);
  });
});

describe("test parseDate()", () => {
  it("handle the en case", () => {
    let date = parseDate("january 1, 2020");
    expect(date.getMonth()).toEqual(0);
    expect(date.getDate()).toEqual(1);
    expect(date.getFullYear()).toEqual(2020);

    date = parseDate("september 30, 2020");
    expect(date.getMonth()).toEqual(8);
    expect(date.getDate()).toEqual(30);
    expect(date.getFullYear()).toEqual(2020);
  });
  it("handle the fr case", () => {
    let date = parseDate("avril 1, 2020");
    expect(date.getMonth()).toEqual(3);
    expect(date.getDate()).toEqual(1);
    expect(date.getFullYear()).toEqual(2020);

    date = parseDate("août 31, 2020");
    expect(date.getMonth()).toEqual(7);
    expect(date.getDate()).toEqual(31);
    expect(date.getFullYear()).toEqual(2020);
  });
  it("handle the limits cases", () => {
    let date = parseDate("blablabla");
    expect(date).toBe(null);
    date = parseDate("");
    expect(date).toBe(null);
    date = parseDate(null);
    expect(date).toBe(null);
  });
});

describe("test getDiscountProfile()", () => {
  afterEach(() => {
    delete process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS;
    // clear the memoized function cache
    getTagsToDiscountProfilesPairing.cache = new memoize.Cache();
    getDiscountProfile.cache = new memoize.Cache();
  });
  it("handle the pairing", () => {
    process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS =
      "tarif1=1,tarif2=2";
    expect(getDiscountProfile(["tarif1"])).toEqual(1);
    expect(getDiscountProfile(["tarif2"])).toEqual(2);
    expect(getDiscountProfile(["tarif3", "tarif1"])).toEqual(1);
    expect(getDiscountProfile(["tarif1", "tarif2"])).toEqual(1);
  });
  it("handle the limits case", () => {
    process.env.NEXT_PUBLIC_TAG_TO_DISCOUNT_PROFILE_PAIRINGS =
      "tarif1=1,tarif2=2";
    expect(getDiscountProfile(null)).toEqual(null);
    expect(getDiscountProfile([])).toEqual(null);
    expect(getDiscountProfile(["tarif3"])).toEqual(null);
    expect(getDiscountProfile(["tarif3", "tarif4"])).toEqual(null);
  });
  it("when there is no env var", () => {
    expect(getDiscountProfile(null)).toEqual(null);
    expect(getDiscountProfile([])).toEqual(null);
    expect(getDiscountProfile(["tarif1"])).toEqual(null);
  });
});

export {};
