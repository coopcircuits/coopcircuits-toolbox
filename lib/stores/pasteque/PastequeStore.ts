import { Store } from "pullstate";

export const PastequeStore = new Store<PastequeStore>({
  pasteque: {
    isLoggedIn: "unknown",
    form: {
      error: null,
    },
    token: null,
  },
  ofn: {
    token: null,
    showLogginForm: false,
  },
  productsList: {
    page: 1,
  },
  ordersList: {
    page: 1,
  },
  paginatedProducts: {},
  import: {
    orders: {
      data: {},
      state: "pending",
      pagination: {
        page: 1,
        pages: null,
        results: 0,
        per_page: 10,
      },
    },
    pagination: {
      page: 1,
      pages: null,
      results: 0,
      per_page: 10,
    },
    loading: "false",
    pagesInError: [],
  },
  exportProducts: {
    status: null,
    products: [],
    variantsCount: 0,
    productsInError: [],
  },
  exportOrders: {
    status: null,
    label: "",
    exportedOrders: [],
    ordersInError: [],
  },
});
