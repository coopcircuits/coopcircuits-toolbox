import { PastequeStore } from "../PastequeStore";

const getAllOrders = (): Array<OFNDetailedOrder> => {
  const orders = PastequeStore.useState((s) => s.import.orders.data);
  return Object.values(orders).flat();
};

export default getAllOrders;
