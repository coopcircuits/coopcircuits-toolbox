import { PastequeStore } from "../PastequeStore";
import getAllProducts from "./getAllProducts";

const importIsCompleted = () => {
  const products = getAllProducts();
  const total = PastequeStore.useState((s) => s.import.pagination.results);
  return products.length === total && total > 0;
};

export default importIsCompleted;
