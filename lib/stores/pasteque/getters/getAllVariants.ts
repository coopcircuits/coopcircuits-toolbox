import { PastequeStore } from "../PastequeStore";
import getAllProducts from "./getAllProducts";

const getAllVariants = (): OFNProduct[] => {
  return getAllProducts().flatMap((value) => value.variants);
};

export default getAllVariants;
