import { PastequeStore } from "../PastequeStore";

const getAllProducts = (): Array<OFNMetaProduct> => {
  const products = PastequeStore.useState((s) => s.paginatedProducts);
  const allProducts = [];
  for (const [key, value] of Object.entries(products)) {
    allProducts.push(...value);
  }
  return allProducts;
};

export default getAllProducts;
