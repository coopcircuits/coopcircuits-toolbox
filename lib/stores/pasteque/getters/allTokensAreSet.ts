import { PastequeStore } from "../PastequeStore";

const allTokensAreSet = (): boolean => {
  const isLoggedIn = PastequeStore.useState((s) => s.pasteque.isLoggedIn);
  const ofnToken = PastequeStore.useState((s) => s.ofn.token);
  return isLoggedIn === "loggedIn" && ofnToken !== null;
};

export default allTokensAreSet;
