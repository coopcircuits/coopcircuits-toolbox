import insertInArray from "./insertInArray";

describe("Test insertInArray()", () => {
  it("handle the default case", async () => {
    const array = [
      { id: 1, label: "1" },
      { id: 2, label: "2" },
    ];
    const ret = insertInArray(array, { id: 3, label: "3" }, (e) => e.id === 3);
    expect(ret).toStrictEqual([
      { id: 1, label: "1" },
      { id: 2, label: "2" },
      { id: 3, label: "3" },
    ]);
  });
  it("handle the default case", async () => {
    const array = [
      { id: 1, label: "1" },
      { id: 2, label: "2" },
    ];
    const ret = insertInArray(array, { id: 2, label: "3" }, (e) => e.id === 2);
    expect(ret).toStrictEqual([
      { id: 1, label: "1" },
      { id: 2, label: "3" },
    ]);
  });
});
