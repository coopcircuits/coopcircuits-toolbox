import getErrorMessage from "./getErrorMessage";

describe("Test getErrorMessage()", () => {
  it("handle the axios error", async () => {
    const error = {
      status: 404,
      statusText: "Page not found",
    };
    const message = getErrorMessage(error);

    expect(message).toStrictEqual("404/Page not found");
  });
  it("handle the axios error with OFN API", async () => {
    const error = {
      status: 404,
      statusText: "Page not found",
      data: {
        error: "Message more explicit",
      },
    };
    const message = getErrorMessage(error);

    expect(message).toStrictEqual("404/Page not found: Message more explicit");
  });
  it("handle the default error", async () => {
    const error = {
      message: "File not found",
    };
    const message = getErrorMessage(error);

    expect(message).toStrictEqual("File not found");
  });
});
