function insertInArray<T>(
  array: Array<T>,
  element: T,
  equalily: (t: T) => boolean
): Array<T> {
  const index = array.findIndex(equalily);
  if (index > -1) {
    array[index] = element;
    return array;
  }
  array.push(element);
  return array;
}

export default insertInArray;
