const getErrorMessage = (error: any): any => {
  if (error.message) {
    return error.message;
  }
  if (error.status && error.statusText) {
    let message = `${error.status}/${error.statusText}`;
    if (error.data && error.data.error) {
      message += `: ${error.data.error}`;
    }
    return message;
  }
  return error;
};

export default getErrorMessage;
