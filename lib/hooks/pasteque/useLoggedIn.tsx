import { useEffect } from "react";
import Router from "next/router";
import { asyncActionPastequeLogin } from "../../../containers/pasteque/login/PastequeLogin";
import LocalStorage from "../../LocalStorage";
import { PastequeStore } from "../../stores/pasteque/PastequeStore";

export const doRegisterOFNToken = async (token: string) => {
  LocalStorage.set("ofn-token", token);
  PastequeStore.update((s) => {
    s.ofn.token = token;
    s.ofn.showLogginForm = false;
  });
};

const useLoggedIn = (redirect: boolean) => {
  const login = LocalStorage.get("pasteque-login");
  const password = LocalStorage.get("pasteque-password");
  const token = LocalStorage.get("ofn-token");
  useEffect(() => {
    (async () => {
      // PASTEQUE
      const result = await asyncActionPastequeLogin.run({
        login,
        password,
      });
      // OFN
      if (token) {
        doRegisterOFNToken(token);
      } else {
        PastequeStore.update((s) => {
          s.ofn.showLogginForm = true;
        });
      }
      if ((result.error || !token) && redirect) {
        Router.push("/pasteque");
      }
    })();
  }, []);
};

export default useLoggedIn;
