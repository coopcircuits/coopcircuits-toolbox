const taxes: PastequeTax[] = [
  { id: 1, label: "Sans TVA", rate: 0 },
  { id: 2, label: "TVA 10%", rate: 0.1 },
  { id: 3, label: "TVA 2,1%", rate: 0.021 },
  { id: 4, label: "TVA 20%", rate: 0.2 },
  { id: 5, label: "TVA 5,5%", rate: 0.055 },
  { id: 7, label: "TVA 21,4%", rate: 0.214 },
];
const order: PastequeOrder = {
  customerName: "test test // 1234",
  date: 1627516800,
  id: {
    id: "1981764",
    source: "coopcircuits.fr",
  },
  reference: "R357353127",
  lines: [
    {
      customTax: 5,
      customUnitPrice: 2.398104,
      product: null,
      productLabel: "Chou rouge BIO",
      quantity: 1,
    },
    {
      customTax: 5,
      customUnitPrice: 1.35545,
      product: null,
      productLabel: "Céleri rave",
      quantity: 1,
    },
    {
      customTax: 5,
      customUnitPrice: 1.876777,
      product: null,
      productLabel: 'Navet "boule d\'or" et navet blanc en mélange BIO',
      quantity: 1,
    },
    {
      customTax: 5,
      customUnitPrice: -14.691943,
      product: null,
      productLabel: "Remboursement",
      quantity: 1,
    },
  ],
  tags: [
    {
      group: "searchkey",
      value: "7309",
    },
  ],
  userName: "",
  discountProfile: null,
};
export { taxes, order };
