import { createAsyncAction, errorResult, successResult } from "pullstate";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import OFNApiClient from "../../../../lib/ofn-api";
import getErrorMessage from "../../../../lib/utils/getErrorMessage";
import LocalStorage from "../../../../lib/LocalStorage";

const getProducts = async ({
  token,
  per_page,
  page,
}: {
  token: string;
  per_page: number;
  page: number;
}) => {
  const ofnApiClient = new OFNApiClient("/api/ofn-proxy", token);
  try {
    const response = await ofnApiClient.getProducts(page, per_page);
    return successResult({
      products: response.products,
      pagination: response.pagination,
    });
  } catch (error) {
    if (error.status === 401) {
      // Handle bad api token
      return errorResult(
        ["BAD_API_KEY"],
        `Bad API key: ${token}`,
        getErrorMessage(error)
      );
    }
    return errorResult(
      ["COULD_NOT_IMPORT_PAGE"],
      `Could not import page: ${page}`,
      getErrorMessage(error)
    );
  }
};

const getProductsAsyncAction = createAsyncAction(getProducts, {
  postActionHook: ({ result, args }) => {
    if (!result.error) {
      PastequeStore.update((s) => {
        s.paginatedProducts = {
          ...s.paginatedProducts,
          [result.payload.pagination.page]: result.payload.products,
        };
        s.import.pagination.page = result.payload.pagination.page;
        s.import.pagination.pages = result.payload.pagination.pages;
        s.import.pagination.per_page = result.payload.pagination.per_page;
        s.import.pagination.results = result.payload.pagination.results;
      });
    } else {
      if (result.tags.includes("COULD_NOT_IMPORT_PAGE")) {
        PastequeStore.update((s) => {
          s.import.pagesInError.push({ args, error: result.errorPayload });
        });
      }
      if (result.tags.includes("BAD_API_KEY")) {
        LocalStorage.delete("ofn-token");
        PastequeStore.update((s) => {
          s.ofn.token = null;
        });
      }
    }
  },
});

export default getProductsAsyncAction;
