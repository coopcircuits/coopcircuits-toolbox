import { createAsyncAction, errorResult, successResult } from "pullstate";
import {
  asyncActionGetAllTaxes,
  categoryMapper,
  exportProductToPasteque,
} from "../../../../lib/OFNToPastequeBridge";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import PastequeApiClient from "../../../../lib/pasteque-api";
import insertInArray from "../../../../lib/utils/insertInArray";
import getErrorMessage from "../../../../lib/utils/getErrorMessage";
import pMap from "p-map";

const exportToPasteque = async ({
  product,
  token,
  pastequeTaxes,
  pastequeCategory,
}: {
  product: OFNProduct;
  token: string;
  pastequeTaxes: PastequeTax[];
  pastequeCategory: PastequeCategory;
}) => {
  try {
    const exportedProduct = await exportProductToPasteque({
      pastequeToken: token,
      product: product,
      pastequeTaxes,
      pastequeCategory,
    });
    return successResult({ products: parseInt(exportedProduct.reference) });
  } catch (error) {
    return errorResult(
      ["COULD_NOT_EXPORT_PRODUCT"],
      `Could not export product: ${product.id}`,
      getErrorMessage(error)
    );
  }
};

export const asyncActionExportToPasteque = createAsyncAction(exportToPasteque, {
  postActionHook: ({ args, result }) => {
    if (!result.error && result.payload) {
      PastequeStore.update((s) => {
        s.exportProducts.products = [
          ...s.exportProducts.products,
          result.payload.products,
        ];
        s.exportProducts.variantsCount++;
        s.exportProducts.productsInError =
          s.exportProducts.productsInError.filter(
            (p) => p.id !== args.product.id
          );
      });
    } else {
      if (result.tags.includes("COULD_NOT_EXPORT_PRODUCT")) {
        PastequeStore.update((s) => {
          s.exportProducts.productsInError = insertInArray(
            s.exportProducts.productsInError,
            {
              id: args.product.id,
              error: result.errorPayload,
              ...args,
            },
            (p) => p.id === args.product.id
          );
        });
      }
    }
  },
});
const exportProducts = async ({
  products,
  token,
}: {
  products: OFNMetaProduct[];
  token: string;
}) => {
  const apiClient = new PastequeApiClient(token);
  PastequeStore.update((s) => {
    s.exportProducts.variantsCount = 0;
    s.exportProducts.status = "pending";
    s.exportProducts.productsInError = [];
  });
  const productsToImport: OFNProduct[] = products.flatMap(
    (currentValue, index, array) => {
      return currentValue.variants;
    }
  );
  try {
    // Get the default taxes
    const pastequeTaxes = (
      await asyncActionGetAllTaxes.run(
        { pastequeToken: token },
        { respectCache: true }
      )
    ).payload.taxes;

    // Create all the needed categories from the producers
    const producers = new Set(productsToImport.map((p) => p.producer_name));
    const existingCategories = await apiClient.getAllCategories();
    const existingCategoriesLabel = existingCategories.map((c) => c.label);
    const categoriesLabelToCreate = Array.from(producers).filter(
      (p) => !existingCategoriesLabel.includes(p)
    );
    const pMapCategoryMapper = async (cat: PastequeCategoryUnsaved) => {
      const category = await apiClient.createCategory(cat);
      existingCategories.push(category);
    };
    await pMap(
      categoriesLabelToCreate.map(categoryMapper),
      pMapCategoryMapper,
      { concurrency: 10 }
    );

    const productMapper = async (product: OFNProduct) =>
      asyncActionExportToPasteque.run({
        product: product,
        token: token,
        pastequeTaxes,
        pastequeCategory: existingCategories.find(
          (c) => c.label === product.producer_name
        ),
      });
    await pMap(productsToImport, productMapper, { concurrency: 10 });
    PastequeStore.update((s) => {
      s.exportProducts.status = "completed";
    });
  } catch (error) {
    return errorResult(
      [],
      "Ne peux pas importer des produits dans pastèque: ",
      error.message || error
    );
  }
};

export default createAsyncAction(exportProducts);
