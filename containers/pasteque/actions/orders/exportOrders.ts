import pMap from "p-map";
import { createAsyncAction, errorResult, successResult } from "pullstate";
import { orderMapper } from "../../../../lib/OFNToPastequeBridge";
import PastequeApiClient from "../../../../lib/pasteque-api";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";

const exportOrders = async ({
  orders,
  token,
}: {
  orders: OFNDetailedOrder[];
  token: string;
}) => {
  try {
    PastequeStore.update((s) => {
      s.exportOrders.label = "Exporting orders...";
      s.exportOrders.status = "running";
      s.exportOrders.exportedOrders = [];
      s.exportOrders.ordersInError = [];
    });
    const apiClient = new PastequeApiClient(token);
    const taxes = await apiClient.getAllTaxes();
    const pastequeOrders = orders.map((o) => orderMapper(o, taxes));

    const pMapOrderMapper = async (order: PastequeOrder) => {
      try {
        const resp = await apiClient.createOrUpdateOrder(order);
        PastequeStore.update((s) => {
          s.exportOrders.exportedOrders.push(order);
        });
      } catch (error) {
        PastequeStore.update((s) => {
          s.exportOrders.ordersInError.push({ order, error });
        });
      }
    };
    await pMap(pastequeOrders, pMapOrderMapper, { concurrency: 10 });
    PastequeStore.update((s) => {
      s.exportOrders.label = `${pastequeOrders.length} orders exported`;
      s.exportOrders.status = "completed";
    });
  } catch (error) {
    return errorResult(
      [],
      "Ne peux pas importer des commande dans pastèque: ",
      error.message || error
    );
  }
};

const exportOrdersAsyncAction = createAsyncAction(exportOrders, {});

export default exportOrdersAsyncAction;
