import importOrdersAsyncAction from "./importPaginatedOrders";
import OFNApiClient from "../../../../lib/ofn-api";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import pMap from "p-map";

const importAllOrders = async ({
  token,
  per_page,
}: {
  token: string;
  per_page: number;
}) => {
  PastequeStore.update((s) => {
    s.import.orders.state = "running";
  });
  const ofnApiClient = new OFNApiClient("/api/ofn-proxy", token);
  // Retrieve the number of pages
  const pages = (await ofnApiClient.getOrders(1, per_page)).pagination.pages;
  const mapper = (index: number) =>
    importOrdersAsyncAction.run({ token, page: index + 1, per_page });
  await pMap(Array.from(Array(pages).keys()), mapper, { concurrency: 1 });
  PastequeStore.update((s) => {
    s.import.orders.state = "completed";
  });
};

export default importAllOrders;
