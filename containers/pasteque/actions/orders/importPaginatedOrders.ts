import { createAsyncAction, errorResult, successResult } from "pullstate";
import LocalStorage from "../../../../lib/LocalStorage";
import OFNApiClient from "../../../../lib/ofn-api";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import getErrorMessage from "../../../../lib/utils/getErrorMessage";

const importOrders = async ({
  token,
  page,
  per_page,
}: {
  token: string;
  page: number;
  per_page: number;
}) => {
  const ofnApiClient = new OFNApiClient("/api/ofn-proxy", token);
  try {
    const response = await ofnApiClient.getOrders(page, per_page);
    return successResult({
      orders: response.orders,
      pagination: response.pagination,
    });
  } catch (error) {
    if (error.status === 401) {
      // Handle bad api token
      return errorResult(
        ["BAD_API_KEY"],
        `Bad API key: ${token}`,
        getErrorMessage(error)
      );
    }
    return errorResult(
      ["COULD_NOT_IMPORT_ORDERS"],
      `Could not import orders`,
      getErrorMessage(error)
    );
  }
};

const importPaginatedOrdersAsyncAction = createAsyncAction(importOrders, {
  postActionHook: ({ result, args }) => {
    if (!result.error) {
      PastequeStore.update((s) => {
        s.import.orders.data = {
          ...s.import.orders.data,
          [result.payload.pagination.page]: result.payload.orders,
        };
        s.import.orders.pagination = result.payload.pagination;
      });
    } else {
      if (result.tags.includes("COULD_NOT_IMPORT_ORDERS")) {
        PastequeStore.update((s) => {});
      }
      if (result.tags.includes("BAD_API_KEY")) {
        LocalStorage.delete("ofn-token");
        PastequeStore.update((s) => {
          s.ofn.token = null;
        });
      }
    }
  },
});

export default importPaginatedOrdersAsyncAction;
