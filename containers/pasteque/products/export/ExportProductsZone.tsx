import getAllProducts from "../../../../lib/stores/pasteque/getters/getAllProducts";
import importIsCompleted from "../../../../lib/stores/pasteque/getters/importIsCompleted";
import Button from "../../../../components/Button";
import ProgressBar from "../../../../components/ProgressBar";
import exportProducts from "../../actions/products/exportProducts";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import getAllVariants from "../../../../lib/stores/pasteque/getters/getAllVariants";
import Errors from "../../../../components/list/Errors";

const ExportProductsZone = ({}) => {
  const products = getAllProducts();
  const importCompleted = importIsCompleted();
  const token = PastequeStore.useState((s) => s.pasteque.token);
  const allVariants = getAllVariants();
  const exportedVariantsCount = PastequeStore.useState(
    (s) => s.exportProducts.variantsCount
  );
  const status = PastequeStore.useState((s) => s.exportProducts.status);
  const loading = status === "pending";
  const completed = status === "completed";
  const productsInError = PastequeStore.useState(
    (s) => s.exportProducts.productsInError
  );
  return (
    <>
      {importCompleted && (
        <div className="w-3/6 m-auto text-center">
          {!loading && !completed && (
            <>
              <div className="mb-5">
                Vous avez {allVariants.length} variantes disponibles pour
                synchronisation avec Pastèque.
              </div>
              <Button
                onClick={(e) =>
                  exportProducts.run({
                    products,
                    token,
                  })
                }
                label="Lancer la synchronisation"
              />
            </>
          )}
          {loading && (
            <ProgressBar
              progress={(exportedVariantsCount * 100) / allVariants.length}
              label={`${exportedVariantsCount} variantes sur ${allVariants.length}`}
            />
          )}
          {completed && (
            <span>
              Transfert vers Pastèque terminé : {exportedVariantsCount}{" "}
              variantes importées sur {allVariants.length}
            </span>
          )}
        </div>
      )}
      {productsInError.length > 0 && (
        <Errors
          type="product"
          objects={productsInError.map((p) => ({
            id: p.product.id.toString(),
            name: p.product.name,
            error: p.error,
          }))}
        />
      )}
    </>
  );
};

export default ExportProductsZone;
