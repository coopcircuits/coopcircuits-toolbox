import Button from "../../../../components/Button";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import exportProducts from "../../actions/products/exportProducts";

const getButtonLabel = (
  state: "default" | "loading",
  count: number,
  total: number
) => {
  if (state === "default") {
    return `Synchroniser ces ${total} produits avec Pastèque`;
  }
  return `Synchronisation en cours : ${count} variantes importées`;
};

const ExportProductsSmallButton = ({}) => {
  const page = PastequeStore.useState((s) => s.productsList.page);
  const products = PastequeStore.useState((s) => s.paginatedProducts)[page];
  const token = PastequeStore.useState((s) => s.pasteque.token);
  const count = PastequeStore.useState((s) => s.exportProducts.variantsCount);
  const [started, finished, result, updating] = exportProducts.useWatch({
    products,
    token,
  });
  const state = started && !finished ? "loading" : "default";
  return (
    <>
      {products && (
        <Button
          size="small"
          onClick={(e) =>
            exportProducts.run({
              products,
              token,
            })
          }
          label={getButtonLabel(state, count, products.length)}
          state={state}
        />
      )}
    </>
  );
};

export default ExportProductsSmallButton;
