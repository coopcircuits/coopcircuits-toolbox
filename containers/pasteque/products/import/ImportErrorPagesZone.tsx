import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import Button from "../../../../components/Button";
import getProductsAsyncAction from "../../actions/products/getProducts";
import { useState } from "react";

const retryAllPagesInError = async (pagesInError: PagesInError[]) => {
  PastequeStore.update((s) => {
    s.import.pagesInError = [];
    s.import.loading = "errors";
  });
  pagesInError.forEach(async (page) => {
    await getProductsAsyncAction.run({ ...page.args });
  });
  PastequeStore.update((s) => {
    s.import.loading = "false";
  });
};

const ImportErrorPagesZone = ({}) => {
  const pagesInError = PastequeStore.useState((s) => s.import.pagesInError);
  const loading = PastequeStore.useState((s) => s.import.loading);
  const [buttonState, setButtonState] = useState<"default" | "loading">(
    "default"
  );
  return (
    <>
      {(pagesInError.length > 0 || loading === "errors") && (
        <>
          <div className="text-3xl">Erreur</div>
          <div className="text-xl text-gray-700">
            <p>
              Certains produits n'ont pas pu être correctement importé depuis la
              plateforme CoopCircuits.
            </p>
            <div className="my-5">
              <Button
                state={buttonState}
                size="small"
                label={`Ressayer`}
                onClick={async (e) => {
                  setButtonState("loading");
                  await retryAllPagesInError(pagesInError);
                  setButtonState("default");
                }}
              />
            </div>
            <p>
              Si le problème persiste, veuillez{" "}
              <a
                href="mailto:support@coopcircuits.fr"
                className="text-cc-orange"
              >
                contacter l’équipe support CoopCircuits
              </a>
              .
            </p>
          </div>
        </>
      )}
    </>
  );
};

export default ImportErrorPagesZone;
