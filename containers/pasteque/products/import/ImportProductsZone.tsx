import ProgressBar from "../../../../components/ProgressBar";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import getProducts from "../../actions/products/getProducts";
import { useEffect } from "react";
import ImportErrorPagesZone from "./ImportErrorPagesZone";
import getAllProducts from "../../../../lib/stores/pasteque/getters/getAllProducts";
import Spinner from "../../../../components/svg/Spinner";
import pMap from "p-map";

const importAllProducts = async ({ token, per_page }) => {
  // Ready, init some var
  PastequeStore.update((s) => {
    s.import.pagesInError = [];
    s.import.loading = "all";
  });

  let pages = 0;
  try {
    const result = await getProducts.run(
      { page: 1, per_page: per_page, token },
      { respectCache: true }
    );
    pages = result.payload.pagination.pages;
  } catch (error) {
    return null;
  }

  const pagesNumber = Array.from(Array(pages + 1).keys()).slice(2);
  const mapper = (index: number) =>
    getProducts.run({ page: index, per_page: per_page, token });
  await pMap(pagesNumber, mapper, { concurrency: 2 });
  PastequeStore.update((s) => {
    s.import.loading = "false";
  });
};

const ImportAllProducts = ({}) => {
  const token = PastequeStore.useState((s) => s.ofn.token);
  const per_page = PastequeStore.useState((s) => s.import.pagination.per_page);
  const total = PastequeStore.useState((s) => s.import.pagination.results);
  const loadingState = PastequeStore.useState((s) => s.import.loading);
  useEffect(() => {
    importAllProducts({ token, per_page });
  }, []);
  const importedSize = getAllProducts().length;
  return (
    <>
      <div className="text-center">
        {loadingState === "all" && (
          <div className="w-3/6 m-auto">
            {!total && (
              <ProgressBar
                progress={null}
                label={
                  <div className="inline-flex items-center ml-1">
                    <Spinner /> Démarrage de l'import...
                  </div>
                }
              />
            )}
            {total > 0 && (
              <ProgressBar
                progress={(importedSize * 100) / total}
                label={`${importedSize} produits sur ${total}`}
              />
            )}
          </div>
        )}
      </div>
      <div>
        {loadingState !== "all" && importedSize !== total && (
          <ImportErrorPagesZone />
        )}
      </div>
    </>
  );
};

export default ImportAllProducts;
