import LoadingRows from "../../../components/list/LoadingRows";
import ProductListComponent from "../../../components/list/ProductList";
import { PastequeStore } from "../../../lib/stores/pasteque/PastequeStore";
import Pagination from "./Pagination";
import getProducts from "../actions/products/getProducts";
import { useEffect } from "react";
import Button from "../../../components/Button";
import Refresh from "../../../components/svg/Refresh";
import ContainerWithChevronForList from "../../../components/ContainerWithChevronForList";

const ProductListTable = () => {
  const page = PastequeStore.useState((s) => s.productsList.page);
  const per_page = PastequeStore.useState((s) => s.import.pagination.per_page);
  const token = PastequeStore.useState((s) => s.ofn.token);
  const exportedProductIds = PastequeStore.useState(
    (s) => s.exportProducts.products
  );
  const paginatedProducts = PastequeStore.useState((s) => s.paginatedProducts);
  const errors = PastequeStore.useState(
    (s) => s.exportProducts.productsInError
  );
  const [started, finished, result, updating] = getProducts.useWatch({
    token,
    per_page,
    page,
  });
  const loading = started && !finished;
  return (
    <ProductListComponent
      loadingComponent={loading ? <LoadingRows n={per_page} /> : null}
      actionsComponent={<ProductListActions />}
      errorComponent={result.error && result.errorPayload && <Error />}
      products={paginatedProducts[page]}
      exportedProducts={exportedProductIds}
      errors={errors}
    />
  );
};

const Error = ({}) => {
  const page = PastequeStore.useState((s) => s.productsList.page);
  const per_page = PastequeStore.useState((s) => s.import.pagination.per_page);
  const token = PastequeStore.useState((s) => s.ofn.token);
  const [started, finished, result, updating] = getProducts.useWatch({
    token,
    per_page,
    page,
  });
  const hasError = result.error && result.errorPayload;
  return (
    <>
      {hasError && (
        <div style={{ height: `${per_page * 2}rem` }} className="flex flex-col">
          <div className="m-auto text-center text-red-400">
            <span className="font-bold">{result.message}</span>
            <pre>{result.errorPayload}</pre>
          </div>
          <div className="m-auto">
            <Button
              size="small"
              state={started && !finished ? "loading" : "default"}
              label="Recharger"
              onClick={(e) => {
                getProducts.clearCache({ token, per_page, page });
                getProducts.run(
                  { token, per_page, page },
                  { respectCache: false }
                );
              }}
            />
          </div>
        </div>
      )}
    </>
  );
};

const ProductListActions = ({}) => {
  const page = PastequeStore.useState((s) => s.productsList.page);
  const per_page = PastequeStore.useState((s) => s.import.pagination.per_page);
  const token = PastequeStore.useState((s) => s.ofn.token);
  const [started, finished, result, updating] = getProducts.useWatch({
    token,
    per_page,
    page,
  });
  const loading = started && !finished;
  return (
    <>
      <div
        onClick={(e) => {
          if (!loading) {
            getProducts.clearCache({ token, per_page, page });
            getProducts.run({ token, per_page, page }, { respectCache: false });
          }
        }}
        className={`${
          loading ? "animate-pulse" : "cursor-pointer"
        } hover:text-gray-700 text-gray-400`}
      >
        <Refresh />
      </div>
    </>
  );
};

const ProductsZone = () => {
  const per_page = PastequeStore.useState((s) => s.import.pagination.per_page);
  const page = PastequeStore.useState((s) => s.productsList.page);
  const token = PastequeStore.useState((s) => s.ofn.token);
  const total = PastequeStore.useState((s) => s.import.pagination.results);
  useEffect(() => {
    getProducts.run({ token, per_page, page });
  }, []);
  if (!total) {
    return null;
  }
  return (
    <ContainerWithChevronForList label="Liste des produits">
      <ProductListTable />
      <div className="mx-2 ">
        <Pagination />
      </div>
    </ContainerWithChevronForList>
  );
};

export default ProductsZone;
