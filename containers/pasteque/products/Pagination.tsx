import { PastequeStore } from "../../../lib/stores/pasteque/PastequeStore";
import PaginationComponent from "../../../components/list/Pagination";
import ExportProductsSmallButton from "./export/ExportProductsSmallButton";
import getProducts from "../actions/products/getProducts";

const Pagination = () => {
  const page = PastequeStore.useState((s) => s.productsList.page);
  const token = PastequeStore.useState((s) => s.ofn.token);
  const products = PastequeStore.useState((s) => s.paginatedProducts)[page];
  const per_page = PastequeStore.useState((s) => s.import.pagination.per_page);
  const pages = PastequeStore.useState((s) => s.import.pagination.pages);
  const results = PastequeStore.useState((s) => s.import.pagination.results);
  const [started, finished, result, updating] = getProducts.useWatch({
    token,
    per_page,
    page,
  });
  if (products == null && pages == null) {
    return null;
  }
  const loading = started && !finished;
  return (
    <PaginationComponent
      page={page}
      results={results}
      pages={pages}
      per_page={per_page}
      onClick={(page) => {
        PastequeStore.update((s) => {
          s.productsList.page = page;
        });
        getProducts.run({ token, per_page, page }, { respectCache: true });
      }}
    >
      {!loading && <ExportProductsSmallButton />}
    </PaginationComponent>
  );
};

export default Pagination;
