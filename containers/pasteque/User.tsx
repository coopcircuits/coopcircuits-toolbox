import UserSVG from "../../components/svg/User";
import { useRef, useState } from "react";
import useOnClickOutside from "../../lib/hooks/useOnClickOutside";
import { PastequeStore } from "../../lib/stores/pasteque/PastequeStore";
import LocalStorage from "../../lib/LocalStorage";
import { NextRouter, useRouter } from "next/router";

const logout = (router: NextRouter) => {
  LocalStorage.delete("pasteque-login");
  LocalStorage.delete("pasteque-password");
  LocalStorage.delete("pasteque-token");
  LocalStorage.delete("ofn-token");
  router.reload();
};

const User = ({}) => {
  const [displayTooltip, setDisplayTooltip] = useState(false);
  const mainRef = useRef();
  useOnClickOutside(mainRef, () => setDisplayTooltip(false));
  const isLoggedIn = PastequeStore.useState((s) => s.pasteque.isLoggedIn);
  const ofnToken = PastequeStore.useState((s) => s.ofn.token);
  const router = useRouter();
  return (
    <>
      {(ofnToken !== null || isLoggedIn === "loggedIn") && (
        <div className="relative" ref={mainRef}>
          <div
            className="border rounded p-1 border-gray-600 hover:border-gray-900 cursor-pointer"
            onClick={(e) => setDisplayTooltip(!displayTooltip)}
          >
            <div className="text-gray-600 hover:text-gray-900">
              <UserSVG />
            </div>
          </div>
          <div
            className={`absolute right-0 mt-2 bg-cc-blue hover:bg-cc-blue-600 text-cc-blue hover:text-cc-blue-600 px-3 py-2 rounded w-40 cursor-pointer shadow-lg ${
              displayTooltip ? "block" : "hidden"
            }`}
            onClick={(e) => logout(router)}
          >
            <svg
              className="absolute  w-4 right-3 ml-3 -top-4 transform rotate-180"
              viewBox="0 0 255 255"
            >
              <polygon
                className="fill-current"
                points="0,0 127.5,127.5 255,0"
              />
            </svg>
            <div className="text-white hover:text-white text-center">
              Se déconnecter
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default User;
