import { PastequeStore } from "../../../lib/stores/pasteque/PastequeStore";
import ExportOrdersZone from "./export/ExportOrdersZone";
import ImportOrdersZone from "./import/ImportOrdersZone";

const OrdersZone = () => {
  const state = PastequeStore.useState((s) => s.import.orders.state);
  return (
    <>
      {state === "completed" && <ExportOrdersZone />}
      <div className="mt-8">
        <ImportOrdersZone />
      </div>
    </>
  );
};

export default OrdersZone;
