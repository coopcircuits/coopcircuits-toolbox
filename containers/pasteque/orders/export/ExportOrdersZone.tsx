import Button from "../../../../components/Button";
import Errors from "../../../../components/list/Errors";
import ProgressBar from "../../../../components/ProgressBar";
import getAllOrders from "../../../../lib/stores/pasteque/getters/getAllOrders";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import exportOrdersAsyncAction from "../../actions/orders/exportOrders";

const ExportErrorOrdersZone = () => {
  const orders = PastequeStore.useState((s) => s.exportOrders.ordersInError);
  if (orders.length === 0) {
    return null;
  }
  return (
    <Errors
      type="order"
      objects={orders.map((o) => ({
        id: o.order.reference,
        name: `${o.order.reference} - ${o.order.customerName}`,
        error: o.error.message,
      }))}
    />
  );
};

const ExportOrdersZone = () => {
  const token = PastequeStore.useState((s) => s.pasteque.token);
  const exportOrders = PastequeStore.useState((s) => s.exportOrders);
  const orders = getAllOrders();
  if (orders && orders.length > 0) {
    return (
      <>
        <div className="w-3/6 m-auto text-center">
          <Button
            label={`Exporter ${orders.length} commandes vers Pastèque`}
            onClick={(e) =>
              exportOrdersAsyncAction.run({
                token,
                orders,
              })
            }
          />
          {exportOrders.status === "running" && (
            <ProgressBar
              progress={
                (exportOrders.exportedOrders.length * 100) / orders.length
              }
              label={`${exportOrders.exportedOrders.length} commande sur ${orders.length}`}
            />
          )}
        </div>
        <ExportErrorOrdersZone />
      </>
    );
  }

  return null;
};

export default ExportOrdersZone;
