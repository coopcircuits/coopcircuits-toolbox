import { useEffect } from "react";
import ContainerWithChevronForList from "../../../../components/ContainerWithChevronForList";
import LoadingRows from "../../../../components/list/LoadingRows";
import OrderList from "../../../../components/list/OrderList";
import Pagination from "../../../../components/list/Pagination";
import Refresh from "../../../../components/svg/Refresh";
import { isExported } from "../../../../lib/OFNToPastequeBridge";
import { PastequeStore } from "../../../../lib/stores/pasteque/PastequeStore";
import importPaginatedOrdersAsyncAction from "../../actions/orders/importPaginatedOrders";
import importAllOrders from "../../actions/orders/importAllOrders";
import ProgressBar from "../../../../components/ProgressBar";
import getAllOrders from "../../../../lib/stores/pasteque/getters/getAllOrders";
import Spinner from "../../../../components/svg/Spinner";

const PaginationContainer = () => {
  const token = PastequeStore.useState((s) => s.ofn.token);
  const orders = PastequeStore.useState((s) => s.import.orders.data);
  const page = PastequeStore.useState((s) => s.ordersList.page);
  const per_page = PastequeStore.useState(
    (s) => s.import.orders.pagination.per_page
  );
  const pages = PastequeStore.useState((s) => s.import.orders.pagination.pages);
  const results = PastequeStore.useState(
    (s) => s.import.orders.pagination.results
  );
  if (orders[page] == null && pages === null) {
    return null;
  }
  return (
    <div className="mx-2 ">
      <Pagination
        page={page}
        results={results}
        pages={pages}
        per_page={per_page}
        onClick={(page) => {
          PastequeStore.update((s) => {
            s.ordersList.page = page;
          });
          importPaginatedOrdersAsyncAction.run(
            {
              token,
              page,
              per_page,
            },
            { respectCache: true }
          );
        }}
      ></Pagination>
    </div>
  );
};

const Actions = ({}) => {
  const page = PastequeStore.useState((s) => s.ordersList.page);
  const per_page = PastequeStore.useState(
    (s) => s.import.orders.pagination.per_page
  );
  const token = PastequeStore.useState((s) => s.ofn.token);
  const [started, finished, result, updating] =
    importPaginatedOrdersAsyncAction.useWatch({
      token,
      per_page,
      page,
    });
  const loading = started && !finished;
  return (
    <>
      <div
        onClick={(e) => {
          if (!loading) {
            importPaginatedOrdersAsyncAction.clearCache({
              token,
              per_page,
              page,
            });
            importPaginatedOrdersAsyncAction.run(
              { token, per_page, page },
              { respectCache: false }
            );
          }
        }}
        className={`${
          loading ? "animate-pulse" : "cursor-pointer"
        } hover:text-gray-700 text-gray-400`}
      >
        <Refresh />
      </div>
    </>
  );
};

const OrdersListWrapper = ({ per_page }: { per_page: number }) => {
  const orders = PastequeStore.useState((s) => s.import.orders.data);
  const token = PastequeStore.useState((s) => s.ofn.token);
  const errors = PastequeStore.useState((s) => s.exportOrders.ordersInError);
  const exportedOrders = PastequeStore.useState(
    (s) => s.exportOrders.exportedOrders
  );
  const orderListPage = PastequeStore.useState((s) => s.ordersList.page);
  const [started, finished, result, updating] =
    importPaginatedOrdersAsyncAction.useWatch({
      token,
      page: orderListPage,
      per_page: per_page,
    });
  const loading = started && !finished;
  return (
    <OrderList
      orders={
        orders &&
        orders[orderListPage] &&
        orders[orderListPage].map((o) => ({
          ...o,
          exported: isExported(o, exportedOrders),
        }))
      }
      errors={errors}
      loadingComponent={loading ? <LoadingRows n={per_page} /> : null}
      actionsComponent={<Actions />}
    />
  );
};

const ImportOrdersZone = () => {
  const token = PastequeStore.useState((s) => s.ofn.token);
  const flattendOrders = getAllOrders();
  const state = PastequeStore.useState((s) => s.import.orders.state);
  const pagination = PastequeStore.useState((s) => s.import.orders.pagination);

  useEffect(() => {
    importAllOrders({ token, per_page: pagination.per_page });
  }, []);

  const [started, finished, result, updating] =
    importPaginatedOrdersAsyncAction.useWatch({
      token,
      page: 1,
      per_page: pagination.per_page,
    });

  return (
    <>
      {state === "running" && (
        <div className="w-3/6 m-auto">
          {pagination.results === 0 && (
            <ProgressBar
              progress={null}
              label={
                <div className="inline-flex items-center ml-1">
                  <Spinner />
                  Démarrage de l'import...
                </div>
              }
            />
          )}
          {pagination.results > 0 && (
            <ProgressBar
              progress={(flattendOrders.length * 100) / pagination.results}
              label={`${flattendOrders.length} commandes sur ${pagination.results}`}
            />
          )}
        </div>
      )}
      {flattendOrders && flattendOrders.length > 0 && (
        <ContainerWithChevronForList label="Liste des commandes">
          <OrdersListWrapper per_page={pagination.per_page} />
          <div className="mx-2 ">
            <PaginationContainer />
          </div>
        </ContainerWithChevronForList>
      )}
      {finished && flattendOrders.length === 0 && (
        <div>⚠️ Il n'y a pas de commande à synchroniser</div>
      )}
    </>
  );
};

export default ImportOrdersZone;
