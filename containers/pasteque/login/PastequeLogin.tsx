import PastequeLoginFormComponent from "../../../components/form/PastequeLoginForm";
import { createAsyncAction, errorResult, successResult } from "pullstate";
import PastequeApiClient from "../../../lib/pasteque-api";
import { PastequeStore } from "../../../lib/stores/pasteque/PastequeStore";
import { useEffect } from "react";
import localStorage from "../../../lib/LocalStorage";

const pastequeApiClient = new PastequeApiClient();

const asyncPastequeLogin = async ({
  login,
  password,
}: {
  login: string;
  password: string;
}) => {
  localStorage.set("pasteque-login", login);
  localStorage.set("pasteque-password", password);
  try {
    const token = await pastequeApiClient.login({
      login,
      password,
    });
    if (token == null || token == "") {
      return errorResult([], "Ne peux pas se loggger à Pastèque.");
    }
    return successResult({
      token: token,
    });
  } catch (error) {
    return errorResult(
      [],
      "Ne peux pas se loggger à Pastèque: ",
      error.message
    );
  }
};

export const asyncActionPastequeLogin = createAsyncAction(asyncPastequeLogin, {
  postActionHook: ({ result }) => {
    if (result.error) {
      PastequeStore.update((s) => {
        s.pasteque.form.error = result.message;
        s.pasteque.isLoggedIn = "loggedOut";
        s.pasteque.token = null;
      });
      localStorage.delete("pasteque-token");
      localStorage.delete("pasteque-login");
      localStorage.delete("pasteque-password");
    } else {
      PastequeStore.update((s) => {
        s.pasteque.token = result.payload.token;
        s.pasteque.isLoggedIn = "loggedIn";
      });
      // No error, save in localStorage
      localStorage.set("pasteque-token", result.payload.token);
    }
  },
});

const PastequeLoginWrapper = () => {
  const isLoggedIn = PastequeStore.useState((s) => s.pasteque.isLoggedIn);
  const error = PastequeStore.useState((s) => s.pasteque.form.error);
  if (isLoggedIn === "loggedIn" || isLoggedIn === "unknown") {
    return null;
  }
  return (
    <PastequeLoginFormComponent
      onSubmit={(login, password) =>
        asyncActionPastequeLogin.run({
          login,
          password,
        })
      }
      error={error}
    />
  );
};
export default PastequeLoginWrapper;
