import LocalStorage from "../../../lib/LocalStorage";
import { PastequeStore } from "../../../lib/stores/pasteque/PastequeStore";
import OFNLogin from "./OFNLogin";
import PastequeLogin from "./PastequeLogin";

const OFNAndPastequeLogin = () => {
  const isLoggedIn = PastequeStore.useState((s) => s.pasteque.isLoggedIn);

  return (
    <>
      {isLoggedIn !== "loggedIn" && <PastequeLogin />}

      <OFNLogin />
    </>
  );
};

export default OFNAndPastequeLogin;
