import OFNTokenFormComponent from "../../../components/form/OFNTokenForm";
import { doRegisterOFNToken } from "../../../lib/hooks/pasteque/useLoggedIn";
import { PastequeStore } from "../../../lib/stores/pasteque/PastequeStore";

const OFNLogin = () => {
  const showLogginForm = PastequeStore.useState((s) => s.ofn.showLogginForm);
  return (
    <>
      {showLogginForm && (
        <div className="mt-20">
          <OFNTokenFormComponent onSubmit={doRegisterOFNToken} />
        </div>
      )}
    </>
  );
};

export default OFNLogin;
