import { useRouter } from "next/router";
import Layout from "../../components/Layout";
import allTokensAreSet from "../../lib/stores/pasteque/getters/allTokensAreSet";
import OFNAndPastequeLogin from "./login";
import Image from "next/image";
import Link from "next/link";
import React from "react";

const PastequeLayout = ({
  subapp,
  children,
}: {
  subapp?: "orders" | "products";
  children: React.ReactChild | React.ReactChild[];
}) => {
  const router = useRouter();
  const displayChildren = allTokensAreSet();
  return (
    <Layout pathname={router.pathname}>
      <div className="my-8 ">
        {!subapp && (
          <div className="max-w-lg mx-auto">
            <Logos size={45} />
          </div>
        )}
        {subapp && (
          <div className="max-w-7xl mx-auto">
            <div className="flex items-center">
              <div className="mr-5">
                <Logos size={30} />
              </div>
              <div className="text-2xl text-gray-400 mr-5">→</div>
              <div className="text-2xl font-bold">
                {subapp === "orders" && "Gestion des commandes"}
                {subapp === "products" && "Gestion des produits"}
              </div>
            </div>
          </div>
        )}
        <div className="mx-auto max-w-xl">
          <OFNAndPastequeLogin />
        </div>
      </div>
      {displayChildren && (
        <div className="max-w-7xl mx-auto mb-8">{children}</div>
      )}
    </Layout>
  );
};

const Logos = ({ size }: { size: 45 | 30 }) => (
  <Link href="/pasteque">
    <a className="text-center block">
      <div className="inline-block">
        <Image
          src="/coopcircuits.png"
          alt="CoopCircuits"
          width={size}
          height={size}
        />
      </div>
      <span
        className={`text-gray-600 ${size === 45 ? "text-5xl" : "text-4xl"}`}
      >
        +
      </span>
      <Image
        src="/pasteque-logo.png"
        alt="Pastèque"
        width={size}
        height={size}
      />
    </a>
  </Link>
);

export default PastequeLayout;
