module.exports = {
  optionalCatchAll: true,
  rewrites: async () => {
    return [
      {
        source: "/:jsadmin",
        destination: "/:jsadmin/index.html",
      },
    ];
  },
};
